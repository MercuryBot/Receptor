/**
* @file InterruptManager.cpp
* @brief Definition file for the InterruptManager class
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @version 1.0.0
* @date February 17th, 2018
*
* The InterruptManager class allows basic manipulation of system interrupts
*
*/

#include "InterruptManager.hpp"

/**
* @brief Basic constructor for the InterruptManager class
*/
InterruptManager::InterruptManager()
{
	m_isInit = false;	
}

/**
* @brief Basic destructor for the InterruptManager class
*/
InterruptManager::~InterruptManager()
{
	
}

/**
* @brief Getter for the m_isInit member variable, tells if the UART port is initialized
* @return Value of the variable (0 is uninitialized, 1 if initialized)
*/
void InterruptManager::setIsInit(bool value)
{
	m_isInit = value;
}

/**
* @brief Setter for the m_isInit member variable
* @param value The new value to set the variable to
*/
bool InterruptManager::isInit(void) const
{
	return m_isInit;
}

/**
* @brief Initializes the interrupts according to the Receptor's configuration 
*/
void InterruptManager::init(void)
{
	// RF module interrupt
	SETBIT(PCICR, PCIE0);
	SETBIT(PCMSK0, PCINT0);
	CLEARBIT(DDRB, DDB0); //IRQ
	SETBIT(PORTB, PORTB0);
	
	// Timer interrupt
	SETBIT(TCCR0A, WGM01);
	OCR0A = 0xF9;
	SETBIT(TIMSK0, OCIE0A);
	SETBIT(TCCR0B, CS00);
	SETBIT(TCCR0B, CS01); //Pre-scaling of 64
	
	sei();
	
	m_isInit = true;
}

/**
* @brief Disables the interrupts
*/
void InterruptManager::stop(void)
{
	cli();
	
	m_isInit = false;
}