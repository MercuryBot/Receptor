/**
* @file InterruptManager.hpp
* @brief Header file for the InterruptManager class
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @version 1.0.0
* @date February 17th, 2018
*
* The InterruptManager class allows basic manipulation of system interrupts
*
*/


#ifndef INTERRUPTMANAGER_HPP_
#define INTERRUPTMANAGER_HPP_

#include <avr/interrupt.h>

#include "Bits.h"

/**
* @class InterruptManager
* @brief allows basic manipulation of system interrupts
*/
class InterruptManager {
	
	private:
	
	/**
	* @brief Value to check if the interrupts are initialized according to the Receptor's configuration
	*/
	bool m_isInit;
	
	public:
	
	/**
	* @brief Basic constructor for the InterruptManager class
	*/
	InterruptManager();
	
	/**
	* @brief Basic destructor for the InterruptManager class
	*/
	~InterruptManager();
	
	/**
	* @brief Getter for the m_isInit member variable, tells if the UART port is initialized
	* @return Value of the variable (0 is uninitialized, 1 if initialized)
	*/
	void setIsInit(bool value);
	
	/**
	* @brief Setter for the m_isInit member variable
	* @param value The new value to set the variable to
	*/
	bool isInit(void) const;
	
	/**
	* @brief Initializes the interrupts according to the Receptor's configuration 
	*/
	void init(void);
	
	/**
	* @brief Disables the interrupts
	*/
	void stop(void);
};

#endif /* INTERRUPTMANAGER_HPP_ */