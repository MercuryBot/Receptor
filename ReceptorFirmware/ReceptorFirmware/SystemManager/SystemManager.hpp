/**
* @file SystemManager.hpp
* @brief Header file for the SystemManager class
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @version 1.0.0
* @date April 18th, 2018
*
* The SystemManager class provides control over basic hardware functionalities.
*
*/

#ifndef SYSTEMMANAGER_HPP_
#define SYSTEMMANAGER_HPP_

#include <stdbool.h>

#include "Operators.h"
#include "ControlGPIO.hpp"
#include "ControlUART.hpp"
#include "ControlSPI.hpp"
#include "InterruptManager.hpp"

/**
* @class SystemManager
* @brief Provides control over basic hardware functionalities
*/
class SystemManager {
	private:
	
	/**
	* @brief Basic constructor for the InterruptManager class
	*/
	SystemManager();
	
	/**
	* @brief Basic destructor for the InterruptManager class
	*/
	~SystemManager();
	
	/**
	* @brief Pointer to the single instance of the class
	*/
	static SystemManager* m_instance;
	
	/**
	* @brief GPIO control member
	*/
	ControlGPIO m_gpio;
	
	/**
	* @brief UART control member
	*/
	ControlUART m_uart;
	
	/**
	* @brief SPI control member
	*/
	ControlSPI m_spi;
	
	/**
	* @brief Interrupt control member
	*/
	InterruptManager m_interrupts;
	
	/**
	* @brief Value to check if all the control members are initialized according to the Receptor's configuration
	*/
	bool m_isInit;
		
	public:
	
	/**
	* @brief Getter of the single instance of the class
	* @return Pointer to the instance 
	*/
	static SystemManager* getInstance();
	
	/**
	* @brief Getter for the m_isInit member variable, tells if the UART port is initialized
	* @return Value of the variable (0 is uninitialized, 1 if initialized)
	*/
	bool isInit(void) const;
	
	/**
	* @brief Setter for the m_isInit member variable
	* @param value The new value to set the variable to
	*/
	void setIsInit(bool value);
	
	/**
	* @brief Getter of the member GPIO control class
	* @return Pointer to the instance 
	*/
	ControlGPIO* gpio(void);
	
	/**
	* @brief Getter of the member UART control class
	* @return Pointer to the instance 
	*/
	ControlUART* uart(void);
	
	/**
	* @brief Getter of the member SPI control class
	* @return Pointer to the instance 
	*/
	ControlSPI* spi(void);
	
	/**
	* @brief Getter of the member interrupts control class
	* @return Pointer to the instance 
	*/	
	InterruptManager* interrupts(void);
	
	/**
	* @brief Initializes the control members according to the Receptor's configuration 
	*/
	void init(void);
};

/**
* @brief Global instance of the SystemManager class
*/
extern SystemManager* syst;

#endif /* SYSTEMMANAGER_HPP_ */