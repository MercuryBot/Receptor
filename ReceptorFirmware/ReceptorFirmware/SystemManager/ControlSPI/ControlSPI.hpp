/**
* @file ControlSPI.hpp
* @brief Header file for the ControlSPI class
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @version 1.0.0
* @date February 12th, 2018
*
* The ControlSPI class allows to control the SPI protocol and auxiliary pins.
*/

#ifndef CONTROLSPI_HPP_
#define CONTROLSPI_HPP_

#ifndef F_CPU
/**
* @def F_CPU
* @brief The external clock value of the processor
*/
#define F_CPU 16000000UL
#endif

//SPI pins
/**
* @def DD_SCK
* @brief The pin number corresponding to the SCK pin in the SPI bus
*/
#define DD_SCK 5
/**
* @def DD_MISO
* @brief The pin number corresponding to the MISO pin in the SPI bus
*/
#define DD_MISO 4
/**
* @def DD_MOSI
* @brief The pin number corresponding to the MOSI pin in the SPI bus
*/
#define DD_MOSI 3
/**
* @def DD_SS
* @brief The pin number corresponding to the SS pin in the SPI bus
*/
#define DD_SS 2
/**
* @def DD_CSN
* @brief The pin number corresponding to the CSN pin in the SPI bus
*/
#define DD_CSN 2
/**
* @def DD_CE
* @brief The pin number corresponding to the CE pin in the SPI bus
*/
#define DD_CE 1
/**
* @def DDR_SPI
* @brief The port in which the SPI bus is (DDRx register configuration)
*/
#define DDR_SPI DDRB
/**
* @def PORT_SPI
* @brief The port in which the SPI bus is (PORTx register configuration)
*/
#define PORT_SPI PORTB

#include <avr/io.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>
#include <util/delay.h>

#include "Bits.h"

class ControlSPI {
	private:
	/**
	* @brief Clock divider to set the SPI bus speed
	*/
	uint32_t m_spiClkDiv;
	
	/**
	* @brief Value to check if the SPI bus is initialized according to the receptor's configuration
	*/
	bool m_init;
	
	protected:
	/**
	* @brief Getter for the SPDR register
	* return Value of the SPDR register
	*/
	uint8_t getSPDR(void) const;
	
	/**
	* @brief Getter for the DDRx register corresponding to the SPI bus' port
	* return Value of the register
	*/
	uint8_t getDDRSPI(void) const;
	
	/**
	* @brief Getter for the SPCR register
	* return Value of the SPCR register
	*/
	uint8_t getSPCR(void) const;
	
	/**
	* @brief Getter of the clock division member variable
	* return Value of the clock division member variable
	*/
	uint32_t getClkDiv(void) const;

	public:
	
	/**
	* @brief Basic constructor for the ControlSPI class
	*/
	ControlSPI();
	
	/**
	* @brief Basic destructor for the ControlSPI class
	*/
	~ControlSPI();
		
	/**
	* @brief Initializes the SPI bus according to the Receptor's configuration in a master configuration
	*/
	void initMaster();
	
	/**
	* @brief Initializes the SPI bus according to the Receptor's configuration in a slave configuration
	*/
	void initSlave();
	
	/**
	* @brief Stops the SPI bus' activity
	*/
	void end();
	
	/**
	* @brief Exchanges a byte over the SPI bus
	* @param c_data Byte to send
	* @return The byte returned from the exchange
	*/
	uint8_t exchByte(uint8_t c_data);
	
	/**
	* @brief Exchanges an array of bytes over the SPI bus
	* @param delay_us Delay in microseconds to wait between each byte exchange
	* @param a_data Array of bytes to send
	* @param len Size of the array to exchange
	* @param a_outData Pointer to the array in which to put the returned bytes
	*/
	void exchBuf(unsigned long delay_us, uint8_t* a_data, size_t len, uint8_t* a_outData);
	
	/**
	* @brief Toggles the CE digital pin on the SPI bus
	* @param state State to set the pin to (0 for low, 1 for high)
	*/
	void toggleCEPin(bool state);
	
	/**
	* @brief Toggles the CSN digital pin on the SPI bus
	* @param state State to set the pin to (0 for low, 1 for high)
	*/
	void toggleCSNPin(bool state);
	
	/**
	* @brief Setter for the clock division parameter, also sets the registers to follow the parameter
	* @param divider Number to which to divide the clock by
	*/
	void setSPIClkDiv(uint8_t divider);
	
	/**
	* @brief Setter for the port in which the SPI bus operates (in DDRx register form)
	* @param reg New value of the register to set
	*/
	void setDDRSPI(uint8_t reg);
	
	/**
	* @brief Setter for the SPDR register
	* @param reg New value of the register to set
	*/
	void setSPDR(uint8_t reg);
	
	/**
	* @brief Setter for the SPCR register
	* @param reg New value of the register to set
	*/
	void setSPCR(uint8_t reg);
	
	/**
	* @brief Getter for the m_init member variable, tells if the SPI bus is initialized
	* @return Value of the variable (0 is uninitialized, 1 if initialized)
	*/
	bool isInit(void) const;
	
	/**
	* @brief Setter for the m_init member variable
	* @param value The new value to set the variable to
	*/
	void setInit(bool value);
};

#endif //CONTROLSPI_HPP_
