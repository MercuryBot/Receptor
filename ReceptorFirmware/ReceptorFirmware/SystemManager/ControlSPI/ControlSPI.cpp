/**
* @file ControlSPI.cpp
* @brief Definition file for the ControlSPI class
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @version 1.0.0
* @date February 12th, 2018
*
* The ControlSPI class allows to control the SPI protocol and auxiliary pins.
*/

#include "ControlSPI.hpp"

ControlSPI::ControlSPI()
{
	m_spiClkDiv = 0;
	m_init = false;
}

ControlSPI::~ControlSPI()
{

}

uint8_t ControlSPI::getSPDR(void) const
{
	return SPDR;
}

uint8_t ControlSPI::getDDRSPI(void) const
{
	return DDR_SPI;
}

uint8_t ControlSPI::getSPCR(void) const
{
	return SPCR;
}

uint32_t ControlSPI::getClkDiv(void) const
{
	return (m_spiClkDiv);
}

void ControlSPI::initMaster()
{
	DDR_SPI = (1<<DD_SCK) | (1<<DD_MOSI) | (1<<DD_CSN) | (1<<DD_CE);
	
	DDR_SPI &= ~(1<<DD_MISO);
	PORT_SPI |= (1<<DD_MISO);
	
	SPCR = (1<<SPE) | (1<<MSTR);
	
	setSPIClkDiv(16);
	
	m_init = true;
	
	SETBIT(PORT_SPI, 2);
	CLEARBIT(PORT_SPI, 1);
}

void ControlSPI::initSlave()
{
	DDR_SPI |= (1<<DD_MISO);
	
	SPCR |= (1<<SPE);
	
	m_init = true;
}

void ControlSPI::end()
{
	SPCR &= ~(1<<SPE);
	
	m_init = false;
}

uint8_t ControlSPI::exchByte(uint8_t c_data)
{
	SPDR = c_data;
	
	while(!(SPSR & (1<<SPIF)));
	
	return (SPDR);
}

void ControlSPI::exchBuf(unsigned long delay_us, uint8_t* a_data, size_t len, uint8_t* a_outData)
{
	for(uint8_t i = 0; i < len; ++i)
	{
		a_outData[i] = this->exchByte(a_data[i]);
		for(unsigned long delay = 0; delay < delay_us; ++delay)
			_delay_us(1);
	}
}

void ControlSPI::toggleCEPin(bool state)
{
	if(state)
		SETBIT(PORT_SPI, DD_CE);
	else
		CLEARBIT(PORT_SPI, DD_CE);
}

void ControlSPI::toggleCSNPin(bool state)
{
	if(state)
		SETBIT(PORT_SPI, DD_CSN);
	else
		CLEARBIT(PORT_SPI, DD_CSN);
}

void ControlSPI::setSPIClkDiv(uint8_t divider)
{
	switch(divider)
	{
		case 2:
			SPCR &= ~(1<<SPR1) & ~(1<<SPR0);
			SPSR |= (1<<SPI2X);
		break;
		case 4:
			SPCR &= ~(1<<SPR1) & ~(1<<SPR0);
			SPSR &= ~(1<<SPI2X);
		break;
		case 8:
			SPCR |= (1<<SPR0);
			SPCR &= ~(1<<SPR1);
			SPSR |= (1<<SPI2X);
		break;
		case 16:
			SPCR |= (1<<SPR0);
			SPCR &= ~(1<<SPR1);
			SPSR &= ~(1<<SPI2X);
		break;
		case 32:
			SPCR &= ~(1<<SPR0);
			SPCR |= (1<<SPR1);
			SPSR |= (1<<SPI2X);
		break;
		case 64:
			SPCR |= (1<<SPR0) | (1<<SPR1);
			SPSR |= (1<<SPI2X);
		break;
		case 128: default:
			SPCR |= (1<<SPR0) | (1<<SPR1);
			SPSR &= ~(1<<SPI2X);
		break;
	}
	
	if(!(divider == 2 || divider == 4|| divider == 8 || divider == 16 || divider == 32 || divider == 64 || divider == 128))
		m_spiClkDiv = 128;
	else
		m_spiClkDiv = divider;
}

void ControlSPI::setDDRSPI(uint8_t reg)
{
	DDR_SPI = reg;
}

void ControlSPI::setSPDR(uint8_t reg)
{
	SPDR = reg;
}

void ControlSPI::setSPCR(uint8_t reg)
{
	SPCR = reg & 0b11000001;
}

bool ControlSPI::isInit() const
{
	return (m_init);
}

void ControlSPI::setInit(bool value)
{
	m_init = value;
}