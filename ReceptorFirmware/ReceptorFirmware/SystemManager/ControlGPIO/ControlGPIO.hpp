/**
* @file ControlGPIO.hpp
* @brief Header file for the ControlGPIO class
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @version 1.0.0
* @date February 22nd, 2018
*
* The ControlGPIO class provides control over basic GPIO pins and ports.
*/


#ifndef CONTROLGPIO_H_
#define CONTROLGPIO_H_

/**
* @def READ
* @brief Value for reading operations
*/
#define READ 0

/**
* @def WRITE
* @brief Value for writing operations
*/
#define WRITE 1

#include <stdbool.h>

#include "Bits.h"

/**
* @class ControlGPIO
* @brief Provides control over basic GPIO pins and ports
*/
class ControlGPIO {
	
	 private:
	 /**
	 * @brief Value to check if the GPIO is initialized according to the receptor's configuration
	 */
	 bool m_isInit;
	 
	 public:
	 
	 /**
	 * @brief Basic constructor for the ControlGPIO class
	 */
	 ControlGPIO();
	 
	 /**
	 * @brief Basic destructor for the ControlGPIO class
	 */
	 ~ControlGPIO();
	 
	 /**
	 * @brief Setter for the m_isInit member variable
	 * @param value The new value to set the variable to
	 */
	 void setIsInit(bool value);
	 
	 /**
	 * @brief Getter for the m_isInit member variable
	 * @return Value of the variable
	 */
	 bool isInit(void) const;
	 
	 /**
	 * @brief Sets the mode (input or output) of a certain pin on a port
	 * @param ddr Port (in ddr defined register form) to target
	 * @param pin Pin to target
	 * @param mode Mode to set the pin to (READ/0 for input, WRITE/1 for output)
	 */
	 void setPinMode(int ddr, int pin, bool mode);
	 
	 /**
	 * @brief Check the mode (input or output) of a certain pin on a port
	 * @param ddr Port (in ddr defined register form) to target
	 * @param pin Pin to target
	 * @return Mode currently set at the pin (READ/0 for input, WRITE/1 for output)
	 */
	 bool getPinMode(int ddr, int pin) const;
	 
	 /**
	 * @brief Sets a pin to high if it is configured in output mode
	 * @param ddr Port (in ddr defined register form) to target
	 * @param pin Pin to target
	 */
	 void setPin(int port, int pin);
	 
	 /**
	 * @brief Sets a pin to high and forces it to output mode
	 * @param port Port (in PORTx defined register form) to target
	 * @param pin Pin to target
	 */
	 void forceSetPin(int port, int pin);
	 
	 /**
	 * @brief Sets a pin to low if it is configured in output mode
	 * @param port Port (in PORTx defined register form) to target
	 * @param pin Pin to target
	 */
	 void resetPin(int port, int pin);
	 
	 /**
	 * @brief Sets a pin to low and forces it to output mode
	 * @param port Port (in PORTx defined register form) to target
	 * @param pin Pin to target
	 */
	 void forceResetPin(int port, int pin);
	 
	 /**
	 * @brief Sets a pin to low or high if it is configured in output mode
	 * @param port Port (in PORTx defined register form) to target
	 * @param pin Pin to target
	 * @param state State to set the pin to (0 for low, 1 for high)
	 */
	 void togglePin(int port, int pin, bool state);
	 
	 /**
	 * @brief Sets a pin to low or high and forces it to output mode
	 * @param port Port (in PORTx defined register form) to target
	 * @param pin Pin to target
	 * @param state State to set the pin to (0 for low, 1 for high)
	 */
	 void forceTogglePin(int port, int pin, bool state);
	 
	 /**
	 * @brief Reads a digital input pin
	 * @param pinset Port (in PINx defined register form) to target
	 * @param pin Pin to target
	 * @return Value of the pin (0 for low, 1 for high)
	 */
	 bool readPin(int pinset, int pin) const;
	 	 
	 /**
	 * @brief Initializes the digital pins configuration to fit the Receptor's configuration
	 */
	 void init(void);
};


#endif /* CONTROLGPIO_H_ */