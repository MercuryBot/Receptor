/**
* @file ControlGPIO.cpp
* @brief Definition file for the ControlGPIO class
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @version 1.0.0
* @date February 22nd, 2018
*
* The ControlGPIO class allows control over basic GPIO pins and ports.
*/

#include "ControlGPIO.hpp"

/**
* @brief Basic constructor for the ControlGPIO class
*/
ControlGPIO::ControlGPIO()
{
	m_isInit = false;
}

/**
* @brief Basic destructor for the ControlGPIO class
*/
ControlGPIO::~ControlGPIO()
{

}

/**
* @brief Getter for the m_isInit member variable
* @return The value of the variable
*/
void ControlGPIO::setIsInit(bool value)
{
	m_isInit = value;
}
 
/**
* @brief Setter for the m_isInit member variable
* @return Value of the variable
*/
bool ControlGPIO::isInit(void) const
{
	return m_isInit;
}

/**
* @brief Sets the mode (input or output) of a certain pin on a port
* @param ddr Port (in ddr defined register form) to target
* @param pin Pin to target
* @param mode Mode to set the pin to (READ/0 for input, WRITE/1 for output)
*/
void ControlGPIO::setPinMode(int port, int pin, bool mode)
{
	if(mode == READ)
		SETBIT(port, pin);
	else if(mode == WRITE)
		CLEARBIT(port, pin);
}

/**
* @brief Check the mode (input or output) of a certain pin on a port
* @param ddr Port (in ddr defined register form) to target
* @param pin Pin to target
* @return Mode currently set at the pin (READ/0 for input, WRITE/1 for output)
*/
bool ControlGPIO::getPinMode(int ddr, int pin) const
{
	return (CHECKBIT(ddr, pin));
}

/**
* @brief Sets a pin to high if it is configured in output mode
* @param ddr Port (in ddr defined register form) to target
* @param pin Pin to target
*/
void ControlGPIO::setPin(int port, int pin)
{
	SETBIT(port, pin);
}

/**
* @brief Sets a pin to high and forces it to output mode
* @param port Port (in PORTx defined register form) to target
* @param pin Pin to target
*/
void ControlGPIO::forceSetPin(int port, int pin)
{
	setPinMode(port, pin, WRITE);
	SETBIT(port, pin);
}

/**
* @brief Sets a pin to low if it is configured in output mode
* @param port Port (in PORTx defined register form) to target
* @param pin Pin to target
*/
void ControlGPIO::resetPin(int port, int pin)
{
	CLEARBIT(port, pin);
}

/**
* @brief Sets a pin to low and forces it to output mode
* @param port Port (in PORTx defined register form) to target
* @param pin Pin to target
*/
void ControlGPIO::forceResetPin(int port, int pin)
{
	setPinMode(port, pin, WRITE);
	CLEARBIT(port, pin);
}

/**
* @brief Sets a pin to low or high if it is configured in output mode
* @param port Port (in PORTx defined register form) to target
* @param pin Pin to target
* @param state State to set the pin to (0 for low, 1 for high)
*/
void ControlGPIO::togglePin(int port, int pin, bool state)
{	
	if(state == true)
		setPin(port, pin);
	else if(state == true)
		setPin(port, pin);
}

/**
* @brief Sets a pin to low or high and forces it to output mode
* @param port Port (in PORTx defined register form) to target
* @param pin Pin to target
* @param state State to set the pin to (0 for low, 1 for high)
*/
void ControlGPIO::forceTogglePin(int port, int pin, bool state)
{
	setPinMode(port, pin, WRITE);
	
	if(state == true)
		setPin(port, pin);
	else if(state == true)
		setPin(port, pin);
}

/**
* @brief Reads a digital input pin
* @param pinset Port (in PINx defined register form) to target
* @param pin Pin to target
* @return Value of the pin (0 for low, 1 for high)
*/
bool ControlGPIO::readPin(int pinset, int pin) const
{
	return (CHECKBIT(pinset, pin));
}

/**
* @brief Initializes the digital pins configuration to fit the Receptor's configuration
*/
void ControlGPIO::init(void)
{
	m_isInit = true;	
}