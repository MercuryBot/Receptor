/**
* @file ControlUART.cpp
* @brief Definition file for the ControlUART class
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @version 1.0.0
* @date February 20th, 2018
*
* The ControlUART manages basic serial communication on the
* ATMega328P's only serial port.
*
*/

#include "ControlUART.hpp"

/**
* @brief Basic constructor for the ControlUART class
*/
ControlUART::ControlUART()
{
	m_baudRate = 0;
	
	m_rxInt = 0;
	m_txInt = 0;
	m_emptyInt = 0;
	m_rxEn = 0;
	m_txEn = 0;
	
	m_dataBits = 0;
	m_syncMode = 0;
	m_parityMode = 0;
	m_stopBits = 0;
	m_clockPhase = 0;
	
	m_init = false;
	
	m_buffer = RingBuffer<char>(RX_BUFFER_LEN);
}

/**
* @brief Basic destructor for the ControlUART class
*/
ControlUART::~ControlUART()
{

}

/**
* @brief Getter of the pointer to the receiving buffer
* @return Pointer to the receiving buffer
*/
RingBuffer<char>* ControlUART::buffer(void)
{
	return &m_buffer;
}

/**
* @brief Setter of the baud rate, also sets corresponding registers
* @param baud New baud rate to set
*/
void ControlUART::setBaudRate(uint16_t baud)
{
	uint16_t baud_prescaler = (((F_CPU / (baud * 16UL))) - 1);
	
	UBRR0H = (uint8_t)(baud_prescaler>>8);
	UBRR0L = (uint8_t)(baud_prescaler);
	
	m_baudRate = baud;
}


/**
* @brief Setter for enabling the receiving interrupt, also sets corresponding registers
* @param data 0 to disable, 1 to enable
*/
void ControlUART::enableRxInt(bool data)
{
	UCSR0B = (UCSR0B & ~(1<<RXCIE0)) | (data<<RXCIE0);
	m_rxInt = data;
}

/**
* @brief Setter for enabling the transmitting interrupt, also sets corresponding registers
* @param data 0 to disable, 1 to enable
*/
void ControlUART::enableTxInt(bool data)
{
	UCSR0B = (UCSR0B & ~(1<<TXCIE0)) | (data<<TXCIE0);
	m_txInt = data;
}

/**
* @brief Setter for enabling the empty receiving buffer interrupt, also sets corresponding registers
* @param data 0 to disable, 1 to enable
*/
void ControlUART::enableEmptyInt(bool data)
{
	UCSR0B = (UCSR0B & ~(1<<UDRIE0)) | (data<<UDRIE0);
	m_emptyInt = data;
}

/**
* @brief Setter for enabling receiving, also sets corresponding registers
* @param data 0 to disable, 1 to enable
*/
void ControlUART::enableRx(bool data)
{
	UCSR0B = (UCSR0B & ~(1<<RXEN0)) | (data<<RXEN0);
	m_rxEn = data;
}

/**
* @brief Setter for enabling transmitting, also sets corresponding registers
* @param data 0 to disable, 1 to enable
*/
void ControlUART::enableTx(bool data)
{
	UCSR0B = (UCSR0B & ~(1<<TXEN0)) | (data<<TXEN0);
	m_txInt = data;
}

/**
* @brief Setter for data bits quantity, also sets corresponding registers
* @param reg Register segment to set (corresponding to defines)
*/
void ControlUART::setDataBits(uint8_t reg)
{
	if(reg != DATA_BIT_5 &&
	reg != DATA_BIT_6 &&
	reg != DATA_BIT_7 &&
	reg != DATA_BIT_8 &&
	reg != DATA_BIT_9)
		return;
	
	UCSR0C = (UCSR0C & ~(0b11<<UCSZ00)) | ((reg<<UCSZ00) & ~(1<<UCSZ02));
	UCSR0B = (UCSR0B & ~(1<<UCSZ02)) | ((reg<<(UCSZ02 - 2)) & ~(0b11));
	
	m_dataBits = reg;
}

/**
* @brief Setter for synchronous/asynchronous mode, also sets corresponding registers
* @param reg Register segment to set (corresponding to defines)
*/
void ControlUART::setSyncMode(uint8_t reg)
{
	if(reg != SYNC_MODE && reg != ASYNC_MODE)
		return;
	
	UCSR0C = (UCSR0C & ~(0b11<<UMSEL00)) | (reg<<UMSEL00);
	m_syncMode = reg;
}

/**
* @brief Setter for the parity mode, also sets corresponding registers
* @param reg Register segment to set (corresponding to defines)
*/
void ControlUART::setParity(uint8_t reg)
{
	if(reg != PARITY_NONE && reg != PARITY_EVEN && reg != PARITY_ODD)
		return;
	
	UCSR0C = (UCSR0C & ~(0b11<<UPM00)) | (reg<<UPM00);
	m_parityMode = reg;
}

/**
* @brief Setter for stop bits quantity, also sets corresponding registers
* @param reg Register segment to set (corresponding to defines)
*/
void ControlUART::setStopBits(uint8_t reg)
{
	if(reg != STOP_BIT_1 && reg != STOP_BIT_2)
		return;
	
	UCSR0C = (UCSR0C & ~(1<<USBS0)) | (reg<<USBS0);
	m_stopBits = reg;
}

/**
* @brief Setter for clock phase, also sets corresponding registers
* @param reg Register segment to set (corresponding to defines)
*/
void ControlUART::setClockPhase(uint8_t reg)
{
	if(reg != XCK_POLARITY_TX && reg != XCK_POLARITY_RX)
	return;
	
	UCSR0C = (UCSR0C & ~(1<<UCPOL0)) | (reg<<UCPOL0);
	m_clockPhase = reg;
}

/**
* @brief Setter for the UBRR0L register
* @param reg Register value to set
*/
void ControlUART::setUBRR0L(uint8_t reg)
{
	UBRR0L = reg;
	m_baudRate = ((F_CPU / ((UBRR0H<<8 | UBRR0L) + 1)) / 16);
}

/**
* @brief Setter for the UBRR0H register
* @param reg Register value to set
*/
void ControlUART::setUBRR0H(uint8_t reg)
{
	UBRR0H = reg;
	m_baudRate = ((F_CPU / ((UBRR0H<<8 | UBRR0L) + 1)) / 16);
}

/**
* @brief Setter for the UCSR0B register
* @param reg Register value to set
*/
void ControlUART::setUCSR0B(uint8_t reg)
{
	UCSR0B = reg;

	m_rxInt = CHECKBIT(reg, RXCIE0);
	m_txInt = CHECKBIT(reg, TXCIE0);
	m_emptyInt = CHECKBIT(reg, UDRIE0);
	m_rxEn = CHECKBIT(reg, RXEN0);
	m_txEn = CHECKBIT(reg, TXEN0);
	m_dataBits = (CHECKBIT(reg, UCSZ02)<<2)|(((0b11<<UCSZ00) & UCSR0C)>>UCSZ00);
}

/**
* @brief Setter for the UCSR0C register
* @param reg Register value to set
*/
void ControlUART::setUCSR0C(uint8_t reg)
{
	UCSR0C = reg;
	
	m_syncMode  = (((0b11<<UMSEL00) & reg)>>UMSEL00);
	m_parityMode = (((0b11<<UPM00) & reg)>>UPM00);
	m_stopBits = CHECKBIT(reg, USBS0);
	m_dataBits = (CHECKBIT(UCSR0B, UCSZ02)<<2)|(((0b11<<UCSZ00) & reg)>>UCSZ00);
	m_clockPhase = CHECKBIT(reg, UCPOL0);
}

/**
* @brief Getter for the currently set baud rate
* @ return Currently set baud rate
*/
uint16_t ControlUART::getBaudRate(void) const
{
	return m_baudRate;
}

/**
* @brief Getter for the currently set receiving interrupt
* @ return 0 if the  interrupt is disabled, 1 if it's enabled
*/
bool ControlUART::isRxInt(void) const
{
	return m_rxInt;
}

/**
* @brief Getter for the currently set transmitting interrupt
* @ return 0 if the interrupt is disabled, 1 if it's enabled
*/
bool ControlUART::istTxInt(void) const
{
	return m_txInt;
}

/**
* @brief Getter for the currently set empty receiving buffer interrupt
* @ return 0 if the interrupt is disabled, 1 if it's enabled
*/
bool ControlUART::isEmptyInt(void) const
{
	return m_emptyInt;
}

/**
* @brief Getter for the currently set receiving permission
* @ return 0 if it's disabled, 1 if it's enabled
*/
bool ControlUART::isRxEn(void) const
{
	return m_rxEn;
}

/**
* @brief Getter for the currently set transmitting permission
* @ return 0 if it's disabled, 1 if it's enabled
*/
bool ControlUART::isTxEn(void) const
{
	return m_txEn;
}

/**
* @brief Getter for the currently set data bits quantity
* @ return Currently set data bits quantity
*/
uint8_t ControlUART::dataBits(void) const
{
	return m_dataBits;
}

/**
* @brief Getter for the currently set synchronous/asynchronous mode
* @ return Currently set synchronous/asynchronous mode
*/
uint8_t ControlUART::syncMode(void) const
{
	return m_syncMode;
}

/**
* @brief Getter for the currently set parity mode
* @ return Currently set parity mode
*/
uint8_t ControlUART::parityMode(void) const
{
	return m_parityMode;
}

/**
* @brief Getter for the currently set stop bits quantity
* @ return Currently set stop bits quantity
*/
uint8_t ControlUART::stopBits(void) const
{
	return m_stopBits;
}

/**
* @brief Getter for the currently set clock phase
* @ return Currently set clock phase
*/
uint8_t ControlUART::clockPhase(void) const
{
	return m_clockPhase;
}

/**
* @brief Initializes the UART port according to the Receptor's configuration 
*/
void ControlUART::init(void)
{
	setBaudRate(DEFAULT_BAUD);
	setUCSR0B(DEFAULT_UCSR0B);
	setUCSR0C(DEFAULT_UCSR0C);

	UCSR0B = (1<<TXEN0)|(1<<RXEN0);
	UCSR0C = (3<<UCSZ00);
	
	enableRxInt(true);
	
	m_init = true;
}

/**
* @brief Initializes the UART port according to the Receptor's configuration 
* @param prescaler Prescaler to set in initialization
* @param ucsr0b UCSR0B register value to set in initialization
* @param ucsr0c UCSR0C register value to set in initialization
*/
void ControlUART::init(uint16_t prescaler, uint8_t ucsr0b, uint8_t ucsr0c)
{
	setUBRR0L((uint8_t)(prescaler));
	setUBRR0H((uint8_t)(prescaler>>8));
	setUCSR0B(ucsr0b);
	setUCSR0C(ucsr0c);
	m_init = 1;
}

/**
* @brief Initializes the UART port according to the Receptor's configuration 
* @param baud Baud rate to set in initialization
*/
void ControlUART::init(uint32_t baud)
{
	setBaudRate(baud);
	setUCSR0B(DEFAULT_UCSR0B);
	setUCSR0C(DEFAULT_UCSR0C);
	m_init = 1;
}

/**
* @brief Allows to check if the UART port is currently receiving
* @return 0 if no, 1 if yes
*/
bool ControlUART::rxReceiving(void)
{
	return (UCSR0A & (1<<RXC0));
}

/**
* @brief Allows to check if the UART port is currently busy on the sending
* @return 0 if no, 1 if yes
*/
bool ControlUART::txBusy(void)
{
	return (!(UCSR0A & (1<<UDRE0)));
}

/**
* @brief Allows to check if the UART port is free to be used
* @return 0 if no, 1 if yes
*/
bool ControlUART::ready(void)
{
	return (m_buffer.getSize() > 0);
}

/**
* @brief Sends a single character over the UART port
* @param car Character to send
*/
void ControlUART::sputc(char car)
{
	while(txBusy());
	UDR0 = car;
}

/**
* @brief Sends a string (null-terminated char array) over the UART port
* @param str String to send
*/
void ControlUART::sputs(const char* str)
{
	while(*str)
	{
		sputc(*str++);
	}
}

/**
* @brief Reads a single character over the UART port
* @return Read character
*/
char ControlUART::sgetch(void)
{
	char tmp;
	
	if(m_buffer.getSize() == 0)
		return 0;
	
	m_buffer.getData(&tmp, 1);
	m_buffer.lopData(1);

	return tmp;
}

/**
* @brief Exchanges a single character over the UART port
* @param car Character to send
* @return Read character
*/
char ControlUART::sgetche(char car)
{
	char tmp = 0;
	
	sputc(car);
	while(!m_buffer.getSize());
	m_buffer.getData(&tmp, 1);
	m_buffer.lopData(1);
	
	return tmp;
}

/**
* @brief Reads a string (null-terminated char array) over the UART port
* @param returnStr Pointer to the buffer in which to put the read string
* @param size Pointer to the variable in which to put the size of the read string
*/
void ControlUART::sgets(char* returnStr, size_t& size)
{	
	if(!m_buffer.getSize())
	{
		returnStr = 0;
		size = 0;
		return;
	}
	
	size = m_buffer.getSize();
	
	m_buffer.getData(returnStr, size);
	m_buffer.lopData(size);
}

/**
* @brief Puts the read bytes over the UART port in the receiving buffer
*/
void ControlUART::fillRxBuf(void)
{
	char tmp = UDR0;

	m_buffer.appendData(&tmp, 1);
}

/**
* @brief Task to execute during the associated interrupt
*/
void ControlUART::intTask(void)
{
	fillRxBuf();
}

/**
* @brief Getter for the m_init member variable, tells if the UART port is initialized
* @return Value of the variable (0 is uninitialized, 1 if initialized)
*/
bool ControlUART::isInit(void) const
{
	return m_init;
}
	
/**
* @brief Setter for the m_init member variable
* @param value The new value to set the variable to
*/
void ControlUART::setInit(bool value)
{
	m_init = value;
}