/**
* @file ControlUART.hpp
* @brief Header file for the ControlUART class
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @version 1.0.0
* @date February 20th, 2018
*
* The ControlUART manages basic serial communication on the
* ATMega328P's only serial port.
*
*/

#ifndef ControlUART_HPP_
#define ControlUART_HPP_

#ifndef F_CPU
/**
* @def F_CPU
* @brief The external clock value of the processor
*/
#define F_CPU 16000000UL
#endif

/**
* @def MAX_BAUDRATE
* @brief Maximum baudrate allowed for the Receptor's configuration
*/
#define MAX_BAUDRATE 9600
/**
* @def MIN_BAUDRATE
* @brief Minimum baudrate possible
*/
#define MIN_BAUDRATE 0

/**
* @def ASYNC_MODE
* @brief Register section value for asynchronous configuration
*/
#define ASYNC_MODE 0b00

/**
* @def SYNC_MODE
* @brief Register section value for synchronous configuration
*/
#define SYNC_MODE 0b01

/**
* @def PARITY_NONE
* @brief Register section value for no parity configuration
*/
#define PARITY_NONE 0b00

/**
* @def PARITY_EVEN
* @brief Register section value for even parity configuration
*/
#define PARITY_EVEN 0b10

/**
* @def PARITY_ODD
* @brief Register section value for odd parity configuration
*/
#define PARITY_ODD 0b11

/**
* @def STOP_BIT_1
* @brief Register section value for 1 stop bit configuration
*/
#define STOP_BIT_1 0
/**
* @def STOP_BIT_2
* @brief Register section value for 2 stop bits configuration
*/
#define STOP_BIT_2 1

/**
* @def DATA_BIT_5
* @brief Register section value for 5 data bits configuration
*/
#define DATA_BIT_5 0x000
/**
* @def DATA_BIT_6
* @brief Register section value for 6 data bits configuration
*/
#define DATA_BIT_6 0b001
/**
* @def DATA_BIT_7
* @brief Register section value for 7 data bits configuration
*/
#define DATA_BIT_7 0b010
/**
* @def DATA_BIT_8
* @brief Register section value for 8 data bits configuration
*/
#define DATA_BIT_8 0b011
/**
* @def DATA_BIT_9
* @brief Register section value for 9 data bits configuration
*/
#define DATA_BIT_9 0b111

/**
* @def XCK_POLARITY_TX
* @brief Register section value for clock polarity on tx pin configuration
*/
#define XCK_POLARITY_TX 0
/**
* @def XCK_POLARITY_RX
* @brief Register section value for clock polarity on rx pin configuration
*/
#define XCK_POLARITY_RX 1

/**
* @def DEFAULT_BAUD
* @brief Default baud rate for the Receptor's configuration
*/
#define DEFAULT_BAUD 9600
/**
* @def DEFAULT_BAUDRATE_PRESCALER
* @brief Default baud rate prescaler for the Receptor's configuration
*/
#define DEFAULT_BAUDRATE_PRESCALER (((F_CPU / (DEFAULT_BAUD * 16UL))) - 1)
/**
* @def DEFAULT_UCSR0B
* @brief Default value of the UCSR0B register for the Receptor's configuration
*/
#define DEFAULT_UCSR0B ((1<<TXEN0)|(1<<RXEN0)|(1<<RXCIE0))
/**
* @def DEFAULT_UCSR0C
* @brief Default value of the UCSR0C register for the Receptor's configuration
*/
#define DEFAULT_UCSR0C (0b11<<UCSZ00)

/**
* @def RX_BUFFER_LEN
* @brief Length of the receiving buffer
*/
#define RX_BUFFER_LEN 128

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "RingBuffer.hpp"
#include "Bits.h"

class ControlUART {
	private:
	
	/**
	* @brief Currently set baud rate of the UART port
	*/
	uint16_t m_baudRate;
	
	/**
	* @brief State of the receiving interrupt (0 for disabled, 1 for enabled)
	*/
	bool m_rxInt;
	
	/**
	* @brief State of the transmitting interrupt (0 for disabled, 1 for enabled)
	*/
	bool m_txInt;
	
	/**
	* @brief State of the empty receiving buffer interrupt (0 for disabled, 1 for enabled)
	*/
	bool m_emptyInt;
	
	/**
	* @brief State of the receiving capacity (0 for disabled, 1 for enabled)
	*/
	bool m_rxEn;
	
	/**
	* @brief State of the transmitting capacity (0 for disabled, 1 for enabled)
	*/
	bool m_txEn;
	
	/**
	* @brief Value to check if the UART port is initialized according to the Receptor's configuration
	*/
	bool m_init;
	
	/**
	* @brief Currently set data bits of the UART port
	*/
	uint8_t m_dataBits;
	
	/**
	* @brief Currently set synchronous/asynchronous mode of the UART port
	*/
	uint8_t m_syncMode;
	
	/**
	* @brief Currently set parity mode of the UART port
	*/
	uint8_t m_parityMode;
	
	/**
	* @brief Currently set stop bits quantity of the UART port
	*/
	uint8_t m_stopBits;
	
	/**
	* @brief Currently set clock phase of the UART port
	*/
	uint8_t m_clockPhase;
	
	/**
	* @brief Receiving buffer of the UART port
	*/	
	RingBuffer<char> m_buffer;
	
	public:
	
	/**
	* @brief Basic constructor for the ControlUART class
	*/
	ControlUART();
	
	/**
	* @brief Basic destructor for the ControlUART class
	*/
	~ControlUART();
	
	/**
	* @brief Getter of the pointer to the receiving buffer
	* @return Pointer to the receiving buffer
	*/
	RingBuffer<char>* buffer(void);
	
	/**
	* @brief Setter of the baud rate, also sets corresponding registers
	* @param baud New baud rate to set
	*/
	void setBaudRate(uint16_t baud);
	
	/**
	* @brief Setter for enabling the receiving interrupt, also sets corresponding registers
	* @param data 0 to disable, 1 to enable
	*/
	void enableRxInt(bool data);
	
	/**
	* @brief Setter for enabling the transmitting interrupt, also sets corresponding registers
	* @param data 0 to disable, 1 to enable
	*/
	void enableTxInt(bool data);
	
	/**
	* @brief Setter for enabling the empty receiving buffer interrupt, also sets corresponding registers
	* @param data 0 to disable, 1 to enable
	*/
	void enableEmptyInt(bool data);
	
	/**
	* @brief Setter for enabling receiving, also sets corresponding registers
	* @param data 0 to disable, 1 to enable
	*/
	void enableRx(bool data);
	
	/**
	* @brief Setter for enabling transmitting, also sets corresponding registers
	* @param data 0 to disable, 1 to enable
	*/
	void enableTx(bool data);
	
	/**
	* @brief Setter for data bits quantity, also sets corresponding registers
	* @param reg Register segment to set (corresponding to defines)
	*/
	void setDataBits(uint8_t reg);
	
	/**
	* @brief Setter for synchronous/asynchronous mode, also sets corresponding registers
	* @param reg Register segment to set (corresponding to defines)
	*/
	void setSyncMode(uint8_t reg);
	
	/**
	* @brief Setter for the parity mode, also sets corresponding registers
	* @param reg Register segment to set (corresponding to defines)
	*/
	void setParity(uint8_t reg);
	
	/**
	* @brief Setter for stop bits quantity, also sets corresponding registers
	* @param reg Register segment to set (corresponding to defines)
	*/
	void setStopBits(uint8_t reg);
	
	/**
	* @brief Setter for clock phase, also sets corresponding registers
	* @param reg Register segment to set (corresponding to defines)
	*/
	void setClockPhase(uint8_t reg);
	
	/**
	* @brief Setter for the UBRR0L register
	* @param reg Register value to set
	*/
	void setUBRR0L(uint8_t reg);
	
	/**
	* @brief Setter for the UBRR0H register
	* @param reg Register value to set
	*/
	void setUBRR0H(uint8_t reg);
	
	/**
	* @brief Setter for the UCSR0B register
	* @param reg Register value to set
	*/
	void setUCSR0B(uint8_t reg);
	
	/**
	* @brief Setter for the UCSR0C register
	* @param reg Register value to set
	*/
	void setUCSR0C(uint8_t reg);
	
	/**
	* @brief Getter for the currently set baud rate
	* @ return Currently set baud rate
	*/
	uint16_t getBaudRate(void) const;
	
	/**
	* @brief Getter for the currently set receiving interrupt
	* @ return 0 if the  interrupt is disabled, 1 if it's enabled
	*/
	bool isRxInt(void) const;
	
	/**
	* @brief Getter for the currently set transmitting interrupt
	* @ return 0 if the interrupt is disabled, 1 if it's enabled
	*/
	bool istTxInt(void) const;
	
	/**
	* @brief Getter for the currently set empty receiving buffer interrupt
	* @ return 0 if the interrupt is disabled, 1 if it's enabled
	*/
	bool isEmptyInt(void) const;
	
	/**
	* @brief Getter for the currently set receiving permission
	* @ return 0 if it's disabled, 1 if it's enabled
	*/
	bool isRxEn(void) const;
	
	/**
	* @brief Getter for the currently set transmitting permission
	* @ return 0 if it's disabled, 1 if it's enabled
	*/
	bool isTxEn(void) const;
	
	/**
	* @brief Getter for the currently set data bits quantity
	* @ return Currently set data bits quantity
	*/
	uint8_t dataBits(void) const;
	
	/**
	* @brief Getter for the currently set synchronous/asynchronous mode
	* @ return Currently set synchronous/asynchronous mode
	*/
	uint8_t syncMode(void) const;
	
	/**
	* @brief Getter for the currently set parity mode
	* @ return Currently set parity mode
	*/
	uint8_t parityMode(void) const;
	
	/**
	* @brief Getter for the currently set stop bits quantity
	* @ return Currently set stop bits quantity
	*/
	uint8_t stopBits(void) const;
	
	/**
	* @brief Getter for the currently set clock phase
	* @ return Currently set clock phase
	*/
	uint8_t clockPhase(void) const;
	
	/**
	* @brief Initializes the UART port according to the Receptor's configuration 
	*/
	void init(void);
	
	/**
	* @brief Initializes the UART port according to the Receptor's configuration 
	* @param prescaler Prescaler to set in initialization
	* @param ucsr0b UCSR0B register value to set in initialization
	* @param ucsr0c UCSR0C register value to set in initialization
	*/
	void init(uint16_t prescaler, uint8_t ucsr0b, uint8_t ucsr0c);
	
	/**
	* @brief Initializes the UART port according to the Receptor's configuration 
	* @param baud Baud rate to set in initialization
	*/
	void init(uint32_t baud);
	
	/**
	* @brief Allows to check if the UART port is currently receiving
	* @return 0 if no, 1 if yes
	*/
	bool rxReceiving(void);
	
	/**
	* @brief Allows to check if the UART port is currently busy on the sending
	* @return 0 if no, 1 if yes
	*/
	bool txBusy(void);
	
	/**
	* @brief Allows to check if the UART port is free to be used
	* @return 0 if no, 1 if yes
	*/
	bool ready(void);
	
	/**
	* @brief Sends a single character over the UART port
	* @param car Character to send
	*/
	void sputc(char car);
	
	/**
	* @brief Sends a string (null-terminated char array) over the UART port
	* @param str String to send
	*/
	void sputs(const char* str);
	
	/**
	* @brief Reads a single character over the UART port
	* @return Read character
	*/
	char sgetch(void);
	
	/**
	* @brief Exchanges a single character over the UART port
	* @param car Character to send
	* @return Read character
	*/
	char sgetche(char car);
	
	/**
	* @brief Reads a string (null-terminated char array) over the UART port
	* @param returnStr Pointer to the buffer in which to put the read string
	* @param size Pointer to the variable in which to put the size of the read string
	*/
	void sgets(char* returnStr, size_t& size);
	
	/**
	* @brief Puts the read bytes over the UART port in the receiving buffer
	*/
	void fillRxBuf(void);
	
	/**
	* @brief Task to execute during the associated interrupt
	*/
	void intTask(void);
	
	/**
	* @brief Getter for the m_init member variable, tells if the UART port is initialized
	* @return Value of the variable (0 is uninitialized, 1 if initialized)
	*/
	bool isInit(void) const;
	
	/**
	* @brief Setter for the m_init member variable
	* @param value The new value to set the variable to
	*/
	void setInit(bool value);
};

#endif /* ControlUART_HPP_ */