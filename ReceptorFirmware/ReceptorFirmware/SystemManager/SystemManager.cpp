/**
* @file SystemManager.cpp
* @brief Definition file for the SystemManager class
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @version 1.0.0
* @date April 18th, 2018
*
* The SystemManager class provides control over basic hardware functionalities
*
*/

#include "SystemManager.hpp"

SystemManager* SystemManager::m_instance;

/**
* @brief Basic constructor for the InterruptManager class
*/
SystemManager::SystemManager()
{
	m_instance = 0;
	
	m_gpio = ControlGPIO();	
	m_uart = ControlUART();
	m_spi = ControlSPI();
	m_interrupts = InterruptManager();
	
	m_isInit = false;
}

/**
* @brief Basic destructor for the InterruptManager class
*/
SystemManager::~SystemManager()
{
	delete m_instance;
}

/**
* @brief Getter of the single instance of the class
* @return Pointer to the instance 
*/
SystemManager* SystemManager::getInstance()
{
	if(!m_instance)
		m_instance = new SystemManager();
			
	return m_instance;
}

/**
* @brief Getter for the m_isInit member variable, tells if the UART port is initialized
* @return Value of the variable (0 is uninitialized, 1 if initialized)
*/
bool SystemManager::isInit(void) const
{
	return m_isInit;
}

/**
* @brief Setter for the m_isInit member variable
* @param value The new value to set the variable to
*/
void SystemManager::setIsInit(bool value)
{
	m_isInit = value;
}

/**
* @brief Getter of the member GPIO control class
* @return Pointer to the instance 
*/
ControlGPIO* SystemManager::gpio(void)
{
	return &m_gpio;
}

/**
* @brief Getter of the member UART control class
* @return Pointer to the instance 
*/
ControlUART* SystemManager::uart(void)
{
	return &m_uart;
}

/**
* @brief Getter of the member SPI control class
* @return Pointer to the instance 
*/
ControlSPI* SystemManager::spi(void)
{
	return &m_spi;
}

/**
* @brief Getter of the member interrupts control class
* @return Pointer to the instance 
*/
InterruptManager* SystemManager::interrupts(void)
{
	return &m_interrupts;
}

/**
* @brief Initializes the control members according to the Receptor's configuration 
*/
void SystemManager::init(void)
{
	m_gpio.init();
	m_uart.init();
	m_spi.initMaster();
	m_interrupts.init();	
	m_isInit = true;
}

/**
* @brief Global instance of the SystemManager class
*/
SystemManager* syst = SystemManager::getInstance();