/**
* @file RingBuffer.tcc
* @brief Definition file the RingBuffer class
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @version 1.0.0
* @date February 14th, 2018
*
* The RingBuffer class is comprised of a circular buffer with management methods.
*
*/

#include "RingBuffer.hpp"

template<class T>
/**
* @brief RingBuffer's default constructor
*/
RingBuffer<T>::RingBuffer()
{
	m_maxSize = DEFAULT_MAX_SIZE;
	m_head = 0;
	m_tail = 0;
	m_size = 0;
	m_buffer = new T[DEFAULT_MAX_SIZE];
}

template<class T>
/**
* @brief RingBuffer's parameter constructor
* @param maxSize Allocated size for the buffer
*/
RingBuffer<T>::RingBuffer(size_t maxSize)
{
	m_maxSize = maxSize;
	m_head = 0;
	m_tail = 0;
	m_size = 0;
	m_buffer = new T[maxSize];
}

template<class T>
/**
* @brief RingBuffer's default destructor
*/
RingBuffer<T>::~RingBuffer()
{
	free(m_buffer);
}

template<class T>
/**
* @brief Put the buffer back to its initial state
*/
void RingBuffer<T>::reset()
{
	m_head = 0;
	m_tail = 0;
	m_size = 0;
}

template<class T>
/**
* @brief Appends data to the buffer
* @param newData Array of data to append
* @param size Size of the array to append
*/
void RingBuffer<T>::appendData(T* newData, size_t size)
{
	bool passedTail = false;
	
	for(size_t i = 0; i < size; ++i)
	{
		m_buffer[m_head]  = newData[i];
		m_head = (m_head + 1) % m_maxSize;
		
		if(m_head == m_tail)
		passedTail = true;
		
		if(!passedTail)
		++m_size;
	}
	
	if(passedTail)
	m_tail = (m_head + 1) % m_maxSize;
}

template<class T>
/**
* @brief Prepends data to the buffer
* @param newData Array of data to prepend
* @param size Size of the array to prepend
*/
void RingBuffer<T>::prependData(T* newData, size_t size)
{
	bool passedHead = false;
	
	for(size_t i = (size - 1); i != -1; --i)
	{
		m_tail = (m_tail == 0) ? (m_maxSize - 1) : --m_tail;
		m_buffer[m_tail] = newData[i];
		
		if(m_tail == m_head)
		passedHead = true;
		
		if(!passedHead && i)
		++m_size;
	}
	
	if(passedHead)
	m_head = (m_tail == 0) ? (m_maxSize - 1) : --m_tail;
}

template<class T>
/**
* @brief Inserts data in the buffer
* @param newData Array of data to insert
* @param size Size of the array to inserts
* @param offset Index from which to insert from
*/
void RingBuffer<T>::insertData(T* newData, size_t size, size_t offset)
{
	size_t tmpBufferSize = (m_head < (m_tail + offset)) ? (m_maxSize - (m_tail + offset)) + m_head : m_head - (m_tail + offset);
	T tmpBuffer[tmpBufferSize];
	
	if((m_tail + offset + tmpBufferSize) % m_maxSize < (m_tail + offset) % m_maxSize)
	{
		memcpy(tmpBuffer, &m_buffer[(m_tail+offset)], (m_maxSize - (m_tail + offset)));
		memcpy(&tmpBuffer[(m_maxSize - (m_tail + offset))], &m_buffer[0], m_head);
	}
	else
	{
		memcpy(tmpBuffer, &m_buffer[(m_tail+offset) % m_maxSize], tmpBufferSize);
	}

	m_head = (m_tail + offset) % m_maxSize;
	
	for(size_t i = 0; i < size; ++i)
	{
		m_buffer[m_head]  = newData[i];
		m_head = (m_head + 1) % m_maxSize;
		
		if(((m_head + 1) % m_maxSize) == m_tail)
		return;
	}
	
	for(size_t i = 0; i < tmpBufferSize; ++i)
	{
		m_buffer[m_head]  = tmpBuffer[i];
		m_head = (m_head + 1) % m_maxSize;
		
		if(((m_head + 1) % m_maxSize) == m_tail)
		return;
	}
	
	m_size += size;
}

template<class T>
/**
* @brief Get data from the buffer
* @param outData Array of data to copy the fetched data to
* @param size Size of the array of data wanted
*/
void RingBuffer<T>::getData(T* outData, size_t size)
{
	for(size_t i = 0; i < size; ++i)
	{
		outData[i] = m_buffer[(m_tail + i) % m_maxSize];
		
		if((m_tail + i) % m_maxSize == m_head)
		return;
	}
}

template<class T>
/**
* @brief Get data from the buffer
* @param outData Array of data to copy the fetched data to
* @param size Size of the array of data wanted
* @param offset Index from which to fetch the data
*/
void RingBuffer<T>::getData(T* outData, size_t size, size_t offset)
{
	for(size_t i = offset; i < (size + offset); ++i)
	{
		outData[i - offset] = m_buffer[(m_tail + i) % m_maxSize];
		
		if((m_tail + i) % m_maxSize == m_head)
		return;
	}
}

template<class T>
/**
* @brief Remove data from the beginning of buffer
* @param size Size of the data to remove
*/
void RingBuffer<T>::lopData(size_t size)
{
	m_tail = (size > m_size ) ? m_head : (m_tail + size) % m_maxSize;
	m_size = (size > m_size ) ? 0 : m_size - size;
}

template<class T>	
/**
* @brief Remove data from the end of buffer
* @param size Size of the data to remove
*/
void RingBuffer<T>::popData(size_t size)
{
	if(size > m_size)
		m_head = m_tail;
	else
		m_head = (m_tail + (m_size - size)) % m_maxSize;

	m_size = (size >= m_size ) ? 0 : m_size - size;
}

template<class T>
/**
* @brief Remove data from a certain index of the buffer
* @param size Size of the data to remove
* @param offset Index from which to remove the data
*/
void RingBuffer<T>::freeData(size_t size, size_t offset)
{
	if(size > m_size)
	{
		reset();
		return;
	}
	
	for(size_t i = 0; i < (m_size  - (size + offset)); ++i)
		m_buffer[(m_tail + i + offset) % m_maxSize] = m_buffer[(m_tail + i + size + offset) % m_maxSize];
	
	m_size -= size;
	m_head = (m_tail + m_size) % m_maxSize;
}

template<class T>
/**
* @brief Setter for the head parameter
* @param head Value to which to set the head to
*/
void RingBuffer<T>::setHead(size_t head)
{
	if(head > m_maxSize)
	return;
	
	m_head = head;
}

template<class T>
/**
* @brief Getter for the head parameter
* @return The value of the head relative to the origin pointer
*/
size_t RingBuffer<T>::getHead(void) const
{
	return m_head;
}

template<class T>
/**
* @brief Setter for the tail parameter
* @param tail Value to which to set the tail to
*/
void RingBuffer<T>::setTail(size_t tail)
{
	if(tail > m_maxSize)
	return;
	
	m_tail = tail;
}

template<class T>
/**
* @brief Getter for the tail parameter
* @return The value of the tail relative to the origin pointer
*/
size_t RingBuffer<T>::getTail(void) const
{
	return m_tail;
}

template<class T>
/**
* @brief Setter for the size parameter
* @param size Value to which to set the size to
*/
void RingBuffer<T>::setSize(size_t size)
{
	if(size > m_maxSize)
	return;
	
	m_size = size;
}

template<class T>
/**
* @brief Getter for the size parameter
* @return The value of the current size of the buffer
*/
size_t RingBuffer<T>::getSize(void) const
{
	return m_size;
}

template<class T>
/**
* @brief Setter for the maximum size parameter
* @param size Value to which to set the maximum size to
*/
void RingBuffer<T>::setMaxSize(size_t size)
{
	if(size > MAX_SAFE_SIZE)
		return;
	
	m_maxSize = size;
	
	free(m_buffer);
	m_buffer = new T[size];
}

template<class T>
/**
* @brief Getter for the maximum size parameter
* @return The value of the maximum size of the buffer
*/
size_t RingBuffer<T>::getMaxSize(void) const
{
	return m_maxSize;
}

template<class T>
/**
* @brief Allows to get the relative index of a sub-buffer
* @param needle The sub-buffer to search for
* @param size The size of the sub-buffer
* @param offset The index from which to search the sub-buffer from
* @return The relative index of the sub-buffer
*/
size_t RingBuffer<T>::getIndexOf(T* needle, size_t size, size_t offset)
{
	size_t foundLead = 0;
	size_t j = 0;
	
	for(size_t i = offset; i < m_size; ++i)
	{
		if(needle[j] == m_buffer[(m_tail + i) % m_maxSize])
		{
			if(!j)
			foundLead = i;
			
			if(j == (size - 1))
			return foundLead;
			
			++j;
		}
		else
		{
			j = 0;
		}
	}
	return 0;
}

template<class T>
/**
* @brief Allows to get the relative index of a sub-buffer
* @param needle The sub-buffer to search for
* @param size The size of the sub-buffer
* @return The relative index of the sub-buffer
*/
size_t RingBuffer<T>::getIndexOf(T* needle, size_t size)
{
	return getIndexOf(needle, size, 0);
}

template<class T>
/**
* @brief Confirms if the buffer contains a sub-buffer
* @param needle The sub-buffer to search for
* @param size The size of the sub-buffer
* @return True if the buffer does contain the sub-buffer, False otherwise
*/
bool RingBuffer<T>::contains(T* needle, size_t size)
{
	if(getIndexOf(needle, size, 0) == 0 && m_buffer[m_tail] != needle[0])
		return false;
	else
		return true;
}

template<class T>
/**
* @brief Confirms if the buffer is empty
* @return True if the buffer is empty, False otherwise
*/
bool RingBuffer<T>::isEmpty(void)
{
	return (!m_size);
}

template<class T>
/**
* @brief Confirms if the buffer is full
* @return True if the buffer is full, False otherwise
*/
bool RingBuffer<T>::isFull(void)
{
	return (m_size == m_maxSize);
}