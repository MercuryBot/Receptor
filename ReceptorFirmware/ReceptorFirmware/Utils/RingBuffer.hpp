/**
* @file RingBuffer.hpp
* @brief Header file the RingBuffer class.
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @version 1.0.0
* @date February 14th, 2018
*
* The RingBuffer class is comprised of a circular buffer with management methods.
*
*/

#ifndef RINGBUFFER_H_
#define RINGBUFFER_H_

/**
* @def DEFAULT_MAX_SIZE
* @brief Default size allocated to buffer in none is specified.
*/
#define DEFAULT_MAX_SIZE 128

/**
* @def MAX_SAFE_SIZE
* @brief Maximum known size to work without encountering problems.
*/
#define MAX_SAFE_SIZE 256

#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "Operators.h"

template<class T>
/**
* @class RingBuffer
* @brief Generic ring buffer template with various methods
*/
class RingBuffer
{
	private:
	/**
	* @brief Maximum size for the instance
	*/
	size_t m_maxSize;
	
	/**
	* @brief Offset of the head against the origin pointer
	*/
	size_t m_head;
	
	/**
	* @brief Offset of the tail against the origin pointer
	*/
	size_t m_tail;
	
	/**
	* @brief Current size of the buffer
	*/
	size_t m_size;
	
	/**
	* @brief Origin pointer to the buffer
	*/
	T* m_buffer;
	
	public:
	
	/**
	* @brief RingBuffer's default constructor
	*/
	RingBuffer();
	
	/**
	* @brief RingBuffer's parameter constructor
	* @param maxSize Allocated size for the buffer
	*/
	RingBuffer(size_t maxSize);

	/**
	* @brief RingBuffer's default destructor
	*/
	~RingBuffer();
	
	/**
	* @brief Put the buffer back to its initial state
	*/
	void reset(void);
	
	/**
	* @brief Appends data to the buffer
	* @param newData Array of data to append
	* @param size Size of the array to append
	*/
	void appendData(T* newData, size_t size);
	
	/**
	* @brief Prepends data to the buffer
	* @param newData Array of data to prepend
	* @param size Size of the array to prepend
	*/
	void prependData(T* newData, size_t size);
	
	/**
	* @brief Inserts data in the buffer
	* @param newData Array of data to insert
	* @param size Size of the array to inserts
	* @param offset Index from which to insert from
	*/
	void insertData(T* newData, size_t size, size_t offset);
	
	/**
	* @brief Get data from the buffer
	* @param outData Array of data to copy the fetched data to
	* @param size Size of the array of data wanted
	*/
	void getData(T* outData, size_t size);
	
	/**
	* @brief Get data from the buffer
	* @param outData Array of data to copy the fetched data to
	* @param size Size of the array of data wanted
	* @param offset Index from which to fetch the data
	*/
	void getData(T* outData, size_t size, size_t offset);
	
	/**
	* @brief Remove data from the beginning of buffer
	* @param size Size of the data to remove
	*/
	void lopData(size_t size);
	
	/**
	* @brief Remove data from the end of buffer
	* @param size Size of the data to remove
	*/
	void popData(size_t size);
	
	/**
	* @brief Remove data from a certain index of the buffer
	* @param size Size of the data to remove
	* @param offset Index from which to remove the data
	*/
	void freeData(size_t size, size_t offset);
	
	/**
	* @brief Setter for the head parameter
	* @param head Value to which to set the head to
	*/
	void setHead(size_t head);
	
	/**
	* @brief Getter for the head parameter
	* @return The value of the head relative to the origin pointer
	*/
	size_t getHead(void) const;
	
	/**
	* @brief Setter for the tail parameter
	* @param tail Value to which to set the tail to
	*/
	void setTail(size_t tail);
	
	/**
	* @brief Getter for the tail parameter
	* @return The value of the tail relative to the origin pointer
	*/
	size_t getTail(void) const;
	
	/**
	* @brief Setter for the size parameter
	* @param size Value to which to set the size to
	*/
	void setSize(size_t size);
	
	/**
	* @brief Getter for the size parameter
	* @return The value of the current size of the buffer
	*/
	size_t getSize(void) const;
	
	/**
	* @brief Setter for the maximum size parameter
	* @param size Value to which to set the maximum size to
	*/
	void setMaxSize(size_t size);
	
	/**
	* @brief Getter for the maximum size parameter
	* @return The value of the maximum size of the buffer
	*/
	size_t getMaxSize(void) const;
	
	/**
	* @brief Allows to get the relative index of a sub-buffer
	* @param needle The sub-buffer to search for
	* @param size The size of the sub-buffer
	* @param offset The index from which to search the sub-buffer from
	* @return The relative index of the sub-buffer
	*/
	size_t getIndexOf(T* needle, size_t size, size_t offset);
	
	/**
	* @brief Allows to get the relative index of a sub-buffer
	* @param needle The sub-buffer to search for
	* @param size The size of the sub-buffer
	* @return The relative index of the sub-buffer
	*/
	size_t getIndexOf(T* needle, size_t size);
	
	/**
	* @brief Confirms if the buffer contains a sub-buffer
	* @param needle The sub-buffer to search for
	* @param size The size of the sub-buffer
	* @return True if the buffer does contain the sub-buffer, False otherwise
	*/
	bool contains(T* needle, size_t size);
	
	/**
	* @brief Confirms if the buffer is empty
	* @return True if the buffer is empty, False otherwise
	*/
	bool isEmpty(void);
	
	/**
	* @brief Confirms if the buffer is full
	* @return True if the buffer is full, False otherwise
	*/
	bool isFull(void);
};

#include "RingBuffer.tcc"

#endif /* RINGBUFFER_H_ */