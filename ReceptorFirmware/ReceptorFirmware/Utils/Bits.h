/**
* @file Bits.h
* @brief File defining basic register manipulating functions at the bit level.
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @version 1.0.0
* @date February 11th, 2018
*
* This file defines basic bit manipulations used for registers.
*
*/


#ifndef BITS_H_
#define BITS_H_

/**
* @def BIT(x)
* @brief Returns a bit with the specified offset in parameter
*/
#define BIT(x) (1<<(x))

/**
* @def SETBITS(x,y)
* @brief Sets the bits at position y in the number x
*/
#define SETBITS(x,y) ((x) |= (y))

/**
* @def CLEARBITS(x,y)
* @brief Resets the bits at position y in the number x
*/
#define CLEARBITS(x,y) ((x) &= (~(y)))

/**
* @def SETBIT(x,y)
* @brief Sets the bit at position y in the number x
*/
#define SETBIT(x,y) SETBITS((x), (BIT((y))))

/**
* @def CLEARBIT(x,y)
* @brief Resets the bit at position y in the number x
*/
#define CLEARBIT(x,y) CLEARBITS((x), (BIT((y))))

/**
* @def CHECKBIT(x,y)
* @brief Returns true if the bit at position y is set in the number x
*/
#define CHECKBIT(x,y) ((x) & (1<<(y)))

#endif /* BITS_H_ */