/**
* @file Operators.h
* @brief Header file containing definitions for non-included c++ operators
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @version 1.0.0
* @date February 12th, 2018
*
* This file declares and defines the ud_std(user defined standard) namespace with its contents.
*
*/
#ifndef OPERATORS_H_
#define OPERATORS_H_

#include <stdlib.h>

/**
* @brief Replacement for the standard C++ operator <i>new</i> for implementation on the AtMega328p. Allocates memory for a given object
* @param objsize Size of the object the allocate memory to
* @return Pointer to the memory block just allocated
*/
inline void* operator new(size_t objsize)
{
	return malloc(objsize);
}

/**
* @brief Replacement for the standard C++ operator <i>delete</i> for implementation on the AtMega328p. Deallocates memory for a given object
* @param obj Pointer to the object to deallocate memory from
*/
inline void operator delete(void* obj)
{
	free(obj);
}

/**
* @brief Replacement for the standard C++ operator <i>new</i> for implementation on the AtMega328p. Allocates memory for a given set of objects
* @param objsize Size of the objects the allocate memory to
* @return Pointer to the memory block just allocated
*/
inline void* operator new[](size_t objsize)
{
	return malloc(objsize);
}

/**
* @brief Replacement for the standard C++ operator <i>delete</i> for implementation on the AtMega328p. Deallocates memory for a given set of objects
* @param obj Pointer to the object to deallocate memory from
*/
inline void operator delete[](void * obj)
{
	free(obj);
}

#endif /* OPERATORS_H_ */