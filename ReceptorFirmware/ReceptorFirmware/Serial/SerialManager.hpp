/**
* @file SerialManager.hpp
* @brief Header file for the SerialManager class
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @version 1.0.0
* @date February 20th, 2018
*
* The SerialManager class helps manage more complex UART/Serial
* communication-related operations with an Operator application.
*
*/

#ifndef SERIALMANAGER_HPP_
#define SERIALMANAGER_HPP_

#include <stdio.h>
#include <string.h>

#include "SerialPacket.h"
#include "SystemManager.hpp"

typedef void task;

class RfManager;

/**
* @class SerialManager
* @brief Helps manage more complex UART/Serial communication-related operations with an Operator application
*/
class SerialManager {
	
	private:
	
	/**
	* @brief Value to check if the connexion with the Operator application is initialized
	*/
	bool m_connexionInit;
	
	protected:
	/**
	* @brief Pointer to an RfManager instance to allow co-dependency with the class
	*/
	RfManager* m_ptr_rf;
	
	public:
	
	/**
	* @brief Basic constructor for the SerialManager class
	*/
	SerialManager();
	
	/**
	* @brief Basic destructor for the SerialManager class
	*/
	~SerialManager();
	
	/**
	* @brief Getter for the member pointer to an RfManager instance
	* @return Pointer to the RfManager instance
	*/
	RfManager* getRf();
	
	/**
	* @brief Setter for the member pointer to an RfManager instance
	* @param ptr_rf New pointer to the RfManager instance
	*/
	void setRf(RfManager* ptr_rf);
	
	/**
	* @brief Allows to check if a new serial communication packet has been received
	* @return 0 if no packet has been received, 1 if one has been received
	*/
	bool receivedPacket(void) const;
	
	/**
	* @brief Extract a packet from the receiving buffer
	* @return Packet structure of the extracted packet
	*/
	SerialPacket extractPacket(void);
	
	/**
	* @brief Extract a parameter from a JSON string
	* @param params JSON string from which to extract the parameter
	* @param param JSON parameter to extract
	* @param outData Pointer to buffer in which to put the extracted parameters
	*/
	void getParamFromJSON(const char* params, const char* param, char* outData);
	
	/**
	* @brief Generates a JSON packet string
	* @param funcName Value of the 'func' parameter in the JSON string
	* @param params JSON parameters to add in the 'params' parameter
	* @param outData Pointer to buffer in which to put the generated JSON string
	*/
	void initJSON(const char* funcName, const char* params, char* outData);
	
	/**
	* @brief Generates a JSON packet string
	* @param funcName Value of the 'func' parameter in the JSON string
	* @param outData Pointer to buffer in which to put the generated JSON string
	*/
	void initJSON(const char* funcName, char* outData);
	
	/**
	* @brief Alters a JSON string to add a parameters under the "params" parameter
	* @param paramName Name of the parameter to add
	* @param paramValue Value of the parameter to add
	* @param outData Pointer to buffer containing the JSON string to alter
	*/
	void addParamToJSON(const char* paramName, const char* paramValue, char* outData);
	
	/**
	* @brief Calls a corresponding reception function based on the "func" parameter value
	* @param func Name of the function to associate to
	* @param params Parameters to pass to the function
	*/
	void callFunction(char* func, char* params);
	
	/**
	* @brief Confirms if the connection with the Operator application is established
	* @return 0 if it's not established, 1 if it is
	*/
	bool isConnInit(void) const;
	
	/**
	* @brief Initializes the connection with the Operator application 
	* @param params JSON string containing the necessary parameters for the connection
	*/
	void setConnInit(char* params);
	
	/**
	* @brief Request a connection with an Operator application
	*/
	void requestInit(void);
	
	/**
	* @brief Artificially sends a packet over the telecommunication network 
	* @param params JSON string containing the necessary parameters for the operation
	*/
	void sendCtrldPacket(char* params);
	
	/**
	* @brief Sends a monitored packet over the serial communication
	* @param packet Packet parameter to add to JSON string
	* @param inOrOut String to add to the JSON string ("IN" if the packet was to the receptor, "OUT" if it was from the receptor)
	*/
	void getMonitoredPacket(char* packet, char* inOrOut);
	
	/**
	* @brief Ping a device over the network
	* @param params JSON string containing the necessary parameters for the operation
	*/	
	void sendPING(char* params);
	
	/**
	* @brief Send a target position with an associated operation to a device over the network
	* @param params JSON string containing the necessary parameters for the operation
	*/	
	void sendTPOS(char* params);
	
	/**
	* @brief Send a current state request to a device over the network
	* @param params JSON string containing the necessary parameters for the operation
	*/	
	void sendRSTT(char* params);
	
	/**
	* @brief Send semi-automatic movement instructions to a device on the network
	* @param params JSON string containing the necessary parameters for the operation
	*/	
	void sendDEPL(char* params);
	
	/**
	* @brief Send an operation mode change to a device on the network
	* @param params JSON string containing the necessary parameters for the operation
	*/	
	void sendMODE(char* params);
	
	/**
	* @brief Send a certain move set to a device on the network
	* @param params JSON string containing the necessary parameters for the operation
	*/	
	void sendMOVE(char* params);
	
	/**
	* @brief Send a sequence to execute to a device on the network
	* @param params JSON string containing the necessary parameters for the operation
	*/	
	void sendSEQU(char* params);
	
	/**
	* @brief Drop connection with a device on the network
	* @param params JSON string containing the necessary parameters for the operation
	*/	
	void sendDROP(char* params);
	
	/**
	* @brief Communicates the information of a new device on network to the Operator application
	* @param addr Address of the new unit
	* @param pipeIdx Index of the pipe to which it is connected
	*/	
	void recvNCON(char* addr, uint8_t pipeIdx);
	
	/**
	* @brief Communicates the information of a device's current position on network to the Operator application
	* @param addr Address of the unit
	* @param code Array of position codes received
	*/
	void recvCPOS(char* addr, uint8_t* code);
	
	/**
	* @brief Communicates the information of a device's low battery alert on network to the Operator application
	* @param addr Address of the unit
	*/
	void recvABAT(char* addr);
	
	/**
	* @brief Communicates the information of a device's battery level on network to the Operator application
	* @param addr Address of the unit
	* @param level Level of battery received
	*/
	void recvLBAT(char* addr, uint32_t level);
	
	/**
	* @brief Communicates the information of a device's state alert on network to the Operator application
	* @param addr Address of the unit
	* @param code Code corresponding to the alert
	*/
	void recvALRT(char* addr, uint8_t code);
	
	/**
	* @brief Communicates the information of a device's current state on network to the Operator application
	* @param addr Address of the unit
	* @param data Array corresponding to the state of the device
	*/
	void recvCSTT(char* addr, uint8_t* data);
	
	/**
	* @brief Communicates the information of a device's current state on network to the Operator application
	* @param addr Address of the unit
	* @param data Array corresponding to the state of the device
	*/
	void recvSDON(char* addr, uint8_t code);
	
	/**
	* @brief Responds to an Operator's request to know the address of a data pipe
	* @param params JSON string containing the necessary parameters for the operation
	*/
	void getPipeAddr_OP(char* params);
	
	/**
	* @brief Responds to an Operator's request to know the number of devices on the network
	*/
	void getDeviceNum_OP(void);
	
	/**
	* @brief Responds to an Operator's request to know the number of devices on a data pipe
	* @param params JSON string containing the necessary parameters for the operation
	*/
	void getDeviceNumOnPipe_OP(char* params);
	
	/**
	* @brief Manually sets a pipe's address on the Rf Module
	* @param params JSON string containing the necessary parameters for the operation
	*/
	void setPipeAddr_OP(char* params);
	
	/**
	* @brief Responds to an Operator's request to know the Receptor's communication ID
	*/
	void requestCommID_OP(void);
	
	/**
	* @brief Manually sets the Receptor's communication ID from the Operator application
	* @param params JSON string containing the necessary parameters for the operation
	*/
	void setCommID_OP(char* params);
	
	/**
	* @brief Set the packet printing from the Operator application
	* @param params JSON string containing the necessary parameters for the operation
	*/
	void setPacketPrinting(char* params);
	
	/**
	* @brief Manually drop the connexion with a unit from the Operator application
	* @param addr Address of the unit to drop form the network
	*/
	void dropDevice(char* addr);
	
	/**
	* @brief Manually stop the physical activity of all units on the network from the Operator application
	* @param addr Address of the unit to drop form the network
	*/
	void stopAllDevices(void);
	
	/**
	* @brief Send an error message to the Operator in case of misunderstood command
	*/
	void returnError_OP(void);
	
	/**
	* @brief Main operating task of the class
	*/
	task mainTask(void);
};

#endif /* SERIALMANAGER_HPP_ */