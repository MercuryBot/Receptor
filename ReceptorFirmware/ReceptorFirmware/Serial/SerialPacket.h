/**
* @file SerialPacket.h
* @brief Header file for the SerialPacket struct
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @version 1.0.0
* @date February 20th, 2018
*
* The SerialPacket struct describes the basic structure of a packet sent 
* over serial port
*
*/

#ifndef SERIALPACKET_H_
#define SERIALPACKET_H_

/**
* @def FUNC_STR_LEN
* @brief Length of the sring allocated to describe its function
*/
#define FUNC_STR_LEN 4

/**
* @struct SerialPacket
* @brief basic structure of a packet sent over serial port
*/
struct SerialPacket {
	/**
	* @brief Buffer associated with the function describing string
	*/
	char func[FUNC_STR_LEN + 1];
	
	/**
	* @brief Buffer associated with the payload of the packet
	*/
	char params[64];
};

#endif /* SERIALPACKET_H_ */