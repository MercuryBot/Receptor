/**
* @file SerialManager.cpp
* @brief Definition file for the SerialManager class
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @version 1.0.0
* @date February 20th, 2018
*
* The SerialManager class helps manage more complex UART/Serial
* communication-related operations with an Operator application.
*
*/

#include "SerialManager.hpp"
#include "RfManager.hpp"

/**
* @brief Basic constructor for the SerialManager class
*/
SerialManager::SerialManager()
{
	m_connexionInit = false;
}

/**
* @brief Basic destructor for the SerialManager class
*/
SerialManager::~SerialManager()
{

}

/**
* @brief Getter for the member pointer to an RfManager instance
* @return Pointer to the RfManager instance
*/
RfManager* SerialManager::getRf()
{
	return m_ptr_rf;
}

/**
* @brief Setter for the member pointer to an RfManager instance
* @param ptr_rf New pointer to the RfManager instance
*/
void SerialManager::setRf(RfManager* ptr_rf)
{
	m_ptr_rf = ptr_rf;
}

/**
* @brief Allows to check if a new serial communication packet has been received
* @return 0 if no packet has been received, 1 if one has been received
*/
bool SerialManager::receivedPacket(void) const
{
	return (syst->uart()->buffer()->contains((char*)"{", 1) && syst->uart()->buffer()->contains((char*)"}}", 2));
}

/**
* @brief Extract a packet from the receiving buffer
* @return Packet structure of the extracted packet
*/
SerialPacket SerialManager::extractPacket(void)
{
	SerialPacket returnPacket;
	char tmpStr[64] = "";

	syst->uart()->buffer()->getData(tmpStr, syst->uart()->buffer()->getSize());
	tmpStr[syst->uart()->buffer()->getSize()] = 0;

	if(!strstr(tmpStr, "func") || !strstr(tmpStr, "params")) //Packet is invalid
	{
		strcpy(returnPacket.func, "INVALID");
		strcpy(returnPacket.params, "INVALID");
		return returnPacket;
	}
	
	snprintf(returnPacket.func, 5, "%s", (strstr(tmpStr, "\"func\":") + strlen("\"func\":") + 1));
	snprintf(returnPacket.params, strstr(tmpStr, "}}") - (strstr(tmpStr, "\"params\":") + strlen("\"params\":")),  (strstr(tmpStr, "\"params\":") + strlen("\"params\":") + 1));
	
	return returnPacket;
}

/**
* @brief Extract a parameter from a JSON string
* @param params JSON string from which to extract the parameter
* @param param JSON parameter to extract
* @param outData Pointer to buffer in which to put the extracted parameters
*/
void SerialManager::getParamFromJSON(const char* params, const char* param, char* outData)
{
	char paramStr[32] = "";
	sprintf(paramStr, "\"%s\":", param);
	
	if(strstr(params, paramStr) != NULL)
	strncpy(outData, (strstr(params, paramStr) + strlen(paramStr) + 1), strstr((strstr(params, paramStr) + strlen(paramStr) + 1), "\"") - (strstr(params, paramStr) + strlen(paramStr) + 1));
}

/**
* @brief Generates a JSON packet string
* @param funcName Value of the 'func' parameter in the JSON string
* @param params JSON parameters to add in the 'params' parameter
* @param outData Pointer to buffer in which to put the generated JSON string
*/
void SerialManager::initJSON(const char* funcName, const char* params, char* outData)
{
	sprintf(outData, "{\"func\":\"%s\",\"params\":{%s}}\r", funcName, params);
}

/**
* @brief Generates a JSON packet string
* @param funcName Value of the 'func' parameter in the JSON string
* @param outData Pointer to buffer in which to put the generated JSON string
*/
void SerialManager::initJSON(const char* funcName, char* outData)
{
	sprintf(outData, "{\"func\":\"%s\",\"params\":{}}\r", funcName);
}

/**
* @brief Alters a JSON string to add a parameters under the "params" parameter
* @param paramName Name of the parameter to add
* @param paramValue Value of the parameter to add
* @param outData Pointer to buffer containing the JSON string to alter
*/
void SerialManager::addParamToJSON(const char* paramName, const char* paramValue, char* outData)
{
	char paramStr[32];
	uint8_t insertIndex = (strstr(outData, "\"params\":{") - outData) + strlen("\"params\":{");
		char tmpStr[256];
		
		sprintf(paramStr, "\"%s\":\"%s\"", paramName, paramValue);
		
	if(!(outData[insertIndex + 1] == '}')) //if there's other parameters already in there
	strcat(paramStr, ",");
	
	strncpy(tmpStr, outData, insertIndex);
	tmpStr[insertIndex] = 0;
	strcat(tmpStr, paramStr);
	strcat(tmpStr, outData + insertIndex);
	sprintf(outData, "%s", tmpStr);
}

/**
* @brief Calls a corresponding reception function based on the "func" parameter value
* @param func Name of the function to associate to
* @param params Parameters to pass to the function
*/
void SerialManager::callFunction(char* func, char* params)
{
	if(strncmp(func, "PCKT", FUNC_STR_LEN) == 0)
		sendCtrldPacket(params);
	else if(strncmp(func, "PING", FUNC_STR_LEN) == 0)
		sendPING(params);
	else if(strncmp(func, "TPOS", FUNC_STR_LEN) == 0)
		sendTPOS(params);
	else if(strncmp(func, "RSTT", FUNC_STR_LEN) == 0)
		sendRSTT(params);
	else if(strncmp(func, "DEPL", FUNC_STR_LEN) == 0)
		sendDEPL(params);
	else if(strncmp(func, "MODE", FUNC_STR_LEN) == 0)
		sendMODE(params);
	else if(strncmp(func, "MOVE", FUNC_STR_LEN) == 0)
		sendMOVE(params);
	else if(strncmp(func, "SEQU", FUNC_STR_LEN) == 0)
		sendSEQU(params);
	else if(strncmp(func, "DROP", FUNC_STR_LEN) == 0)
		sendDROP(params);
	else if(strncmp(func, "GPAO", FUNC_STR_LEN) == 0)
		getPipeAddr_OP(params);
	else if(strncmp(func, "GDNO", FUNC_STR_LEN) == 0)
		getDeviceNum_OP();
	else if(strncmp(func, "GDPO", FUNC_STR_LEN) == 0)
		getDeviceNumOnPipe_OP(params);
	else if(strncmp(func, "SPAO", FUNC_STR_LEN) == 0)
		setPipeAddr_OP(params);
	else if(strncmp(func, "RCID", FUNC_STR_LEN) == 0)
		requestCommID_OP();
	else if(strncmp(func, "SCID", FUNC_STR_LEN) == 0)
		setCommID_OP(params);
	else if(strncmp(func, "SPPA", FUNC_STR_LEN) == 0)
		setPacketPrinting(params);
	else if(strncmp(func, "INIT", FUNC_STR_LEN) == 0)
		setConnInit(params);
	else if(strncmp(func, "STOP", FUNC_STR_LEN) == 0)
		stopAllDevices();
	else
		returnError_OP(); //The code is invalid
}

/**
* @brief Confirms if the connection with the Operator application is established
* @return 0 if it's not established, 1 if it is
*/
bool SerialManager::isConnInit(void) const
{
	return m_connexionInit;
}

/**
* @brief Initializes the connection with the Operator application 
* @param params JSON string containing the necessary parameters for the connection
*/
void SerialManager::setConnInit(char* params)
{
	char commId[2];
	
	getParamFromJSON(params, "ID", commId);
	m_ptr_rf->setCommID(commId[0]);
	
	m_connexionInit = true;
}

/**
* @brief Request a connection with an Operator application
*/
void SerialManager::requestInit(void)
{
	char packet[128];
	
	initJSON("INIT", packet);
	syst->uart()->sputs(packet);
}

/**
* @brief Artificially sends a packet over the telecommunication network 
* @param params JSON string containing the necessary parameters for the operation
*/
void SerialManager::sendCtrldPacket(char* params)
{
	char packet[33];
	char address[6];

	getParamFromJSON(params, "packet", packet);
	getParamFromJSON(params, "address", address);
	
	m_ptr_rf->sendData(address, packet);
}

/**
* @brief Sends a monitored packet over the serial communication
* @param packet Packet parameter to add to JSON string
* @param inOrOut String to add to the JSON string ("IN" if the packet was to the receptor, "OUT" if it was from the receptor)
*/
void SerialManager::getMonitoredPacket(char* receivedPacket, char* inOrOut)
{
	char packet[128];
	
	initJSON("PPKT", receivedPacket);
	addParamToJSON("packet", receivedPacket, packet);
	addParamToJSON("direction", inOrOut, packet);
	
	syst->uart()->sputs(packet);
}

/**
* @brief Ping a device over the network
* @param params JSON string containing the necessary parameters for the operation
*/	
void SerialManager::sendPING(char* params)
{
	char address[6];
	
	getParamFromJSON(params, "address", address);
	
	m_ptr_rf->sendPING(address);
}

/**
* @brief Send a target position with an associated operation to a device over the network
* @param params JSON string containing the necessary parameters for the operation
*/	
void SerialManager::sendTPOS(char* params)
{
	char address[6];
	char sequ[2];
	char sequSpec[2];
	char code_1[2];
	char code_2[2];
	char code_3[2];
	char codes[3];
	
	getParamFromJSON(params, "address", address);
	getParamFromJSON(params, "sequ", sequ);
	getParamFromJSON(params, "spec", sequSpec);
	getParamFromJSON(params, "code_1", code_1);
	getParamFromJSON(params, "code_2", code_2);
	getParamFromJSON(params, "code_3", code_3);
	codes[0] = atoi(code_1);
	codes[1] = atoi(code_2);
	codes[2] = atoi(code_3);
	
	m_ptr_rf->sendTPOS(address,sequ[0], sequSpec[0], codes);
}

/**
* @brief Send a current state request to a device over the network
* @param params JSON string containing the necessary parameters for the operation
*/	
void SerialManager::sendRSTT(char* params)
{
	char address[6];
	
	getParamFromJSON(params, "address", address);
	
	m_ptr_rf->sendRSTT(address);
}

/**
* @brief Send semi-automatic movement instructions to a device on the network
* @param params JSON string containing the necessary parameters for the operation
*/	
void SerialManager::sendDEPL(char* params)
{
	char address[6];
	char move_1[4];
	char move_2[4];
	char move_3[4];
	char move_4[4];
	
	getParamFromJSON(params, "address", address);
	getParamFromJSON(params, "move_1", move_1);
	getParamFromJSON(params, "move_2", move_2);
	getParamFromJSON(params, "move_3", move_3);
	getParamFromJSON(params, "move_4", move_4);
	
	m_ptr_rf->sendDEPL(address, atoi(move_1), atoi(move_2), atoi(move_3), atoi(move_4));
}

/**
* @brief Send an operation mode change to a device on the network
* @param params JSON string containing the necessary parameters for the operation
*/	
void SerialManager::sendMODE(char* params)
{
	char address[6];
	char a_mode[4];
	uint8_t mode;
	
	getParamFromJSON(params, "address", address);
	getParamFromJSON(params, "mode", a_mode);
	mode = atoi(a_mode);

	m_ptr_rf->sendMODE(address, mode);
}

/**
* @brief Send a certain move set to a device on the network
* @param params JSON string containing the necessary parameters for the operation
*/	
void SerialManager::sendMOVE(char* params)
{
	char address[6];
	char moveSet[16];
	
	getParamFromJSON(params, "address", address);
	getParamFromJSON(params, "moveset", moveSet);

	m_ptr_rf->sendMOVE(address, (uint8_t*)moveSet);
}

/**
* @brief Send a sequence to execute to a device on the network
* @param params JSON string containing the necessary parameters for the operation
*/	
void SerialManager::sendSEQU(char* params)
{
	char address[6];
	char a_sequCode[4];
	char a_sequSpecific[4];
	char sequCode;
	char sequSpecific;
	
	getParamFromJSON(params, "address", address);
	getParamFromJSON(params, "code", a_sequCode);
	getParamFromJSON(params, "specific", a_sequSpecific);
	sequCode = atoi(a_sequCode);
	sequSpecific = atoi(a_sequSpecific);
	
	m_ptr_rf->sendSEQU(address, sequCode, sequSpecific);
}

/**
* @brief Drop connection with a device on the network
* @param params JSON string containing the necessary parameters for the operation
*/
void SerialManager::sendDROP(char* params)
{
	char address[6];
	
	getParamFromJSON(params, "address", address);
	
	m_ptr_rf->sendDROP(address);
}

/**
* @brief Communicates the information of a new device on network to the Operator application
* @param addr Address of the new unit
* @param pipeIdx Index of the pipe to which it is connected
*/	
void SerialManager::recvNCON(char* addr, uint8_t pipeIdx)
{
	char packet[128];
	char a_pipeIdx[4];
	
	itoa(pipeIdx, a_pipeIdx, 10);
	
	initJSON("NCON", packet);
	addParamToJSON("address", addr, packet);
	addParamToJSON("pipe", a_pipeIdx, packet);
	syst->uart()->sputs(packet);
}

/**
* @brief Communicates the information of a device's current position on network to the Operator application
* @param addr Address of the unit
* @param code Array of position codes received
*/
void SerialManager::recvCPOS(char* addr, uint8_t* code)
{
	char packet[128];
	char a_code_1[4];
	char a_code_2[4];
	char a_code_3[4];
	
	itoa(code[0], a_code_1, 10);
	itoa(code[1], a_code_2, 10);
	itoa(code[2], a_code_3, 10);
	
	initJSON("CPOS", packet);
	addParamToJSON("address", addr, packet);
	addParamToJSON("code_1", a_code_1, packet);
	addParamToJSON("code_2", a_code_2, packet);
	addParamToJSON("code_3", a_code_3, packet);
	syst->uart()->sputs(packet);
}

/**
* @brief Communicates the information of a device's low battery alert on network to the Operator application
* @param addr Address of the unit
*/
void SerialManager::recvABAT(char* addr)
{
	char packet[128];
	
	initJSON("ABAT", packet);
	addParamToJSON("address", addr, packet);
	syst->uart()->sputs(packet);
}

/**
* @brief Communicates the information of a device's battery level on network to the Operator application
* @param addr Address of the unit
* @param level Level of battery received
*/
void SerialManager::recvLBAT(char* addr, uint32_t level)
{
	char packet[128];
	char a_level[11];
	
	itoa(level, a_level, 10);
	
	initJSON("LBAT", packet);
	addParamToJSON("address", addr, packet);
	addParamToJSON("level", a_level, packet);
	syst->uart()->sputs(packet);
}

/**
* @brief Communicates the information of a device's state alert on network to the Operator application
* @param addr Address of the unit
* @param code Code corresponding to the alert
*/
void SerialManager::recvALRT(char* addr, uint8_t code)
{
	char packet[128];
	char a_code[4];
	
	itoa(code, a_code, 10);
	
	initJSON("ALRT", packet);
	addParamToJSON("address", addr, packet);
	addParamToJSON("code", a_code, packet);
	syst->uart()->sputs(packet);
}

/**
* @brief Communicates the information of a device's current state on network to the Operator application
* @param addr Address of the unit
* @param data Array corresponding to the state of the device
*/
void SerialManager::recvCSTT(char* addr, uint8_t* data)
{
	char packet[128];
	char a_data[17];
	
	memcpy(a_data, (char*)data, 16);
	a_data[16] = 0;
	
	initJSON("CSTT", packet);
	addParamToJSON("address", addr, packet);
	addParamToJSON("data", a_data, packet);
	syst->uart()->sputs(packet);
}

/**
* @brief Communicates the information of a device's current state on network to the Operator application
* @param addr Address of the unit
* @param data Array corresponding to the state of the device
*/
void SerialManager::recvSDON(char* addr, uint8_t code)
{
	char packet[128];
	char a_code[4];
	
	itoa(code, a_code, 10);
	
	initJSON("SDON", packet);
	addParamToJSON("address", addr, packet);
	addParamToJSON("code", a_code, packet);
	syst->uart()->sputs(packet);
}

/**
* @brief Responds to an Operator's request to know the address of a data pipe
* @param params JSON string containing the necessary parameters for the operation
*/
void SerialManager::getPipeAddr_OP(char* params)
{
	char address[6] = {0};
	char a_index[4];
	char packet[128];
	uint8_t index;

	getParamFromJSON(params, "index", a_index);
	
	index = atoi(a_index);
	ud_std::clamp(index, 0, 5);
	
	m_ptr_rf->readReg(RX_ADDR_P0 + index, 5, (uint8_t*)address);
	
	initJSON("GPAO", packet);
	addParamToJSON("address", address, packet);
	syst->uart()->sputs(packet);
}

/**
* @brief Responds to an Operator's request to know the number of devices on the network
*/
void SerialManager::getDeviceNum_OP(void)
{
	uint8_t num = m_ptr_rf->getDeviceNum();
	char a_num[4];
	char packet[128];
	
	itoa(num, a_num, 10);
	
	initJSON("GDNO", packet);
	addParamToJSON("num", a_num, packet);
	syst->uart()->sputs(packet);
}

/**
* @brief Responds to an Operator's request to know the number of devices on a data pipe
* @param params JSON string containing the necessary parameters for the operation
*/
void SerialManager::getDeviceNumOnPipe_OP(char* params)
{
	char a_index[4];
	uint8_t num;
	char a_num[4] = "";
	char packet[128];
	
	getParamFromJSON(params, "index", a_index);
	num = m_ptr_rf->getDeviceNumOnPipe(atoi(a_index));
	
	itoa(num, a_num, 10);
	initJSON("GDPO", packet);
	addParamToJSON("num", a_num, packet);
	syst->uart()->sputs(packet);
}

/**
* @brief Manually sets a pipe's address on the Rf Module
* @param params JSON string containing the necessary parameters for the operation
*/
void SerialManager::setPipeAddr_OP(char* params)
{
	char address[6];
	char a_index[4];
	
	getParamFromJSON(params, "address", address);
	getParamFromJSON(params, "index", a_index);
	
	m_ptr_rf->setPipe(atoi(a_index), address);
}

/**
* @brief Responds to an Operator's request to know the Receptor's communication ID
*/
void SerialManager::requestCommID_OP(void)
{
	char commID[2] = {m_ptr_rf->getCommID(), 0};
	char packet[128];

	initJSON("RCID", packet);
	addParamToJSON("ID", commID, packet);
	syst->uart()->sputs(packet);
}

/**
* @brief Manually sets the Receptor's communication ID from the Operator application
* @param params JSON string containing the necessary parameters for the operation
*/
void SerialManager::setCommID_OP(char* params)
{
	char commId[2];
	
	getParamFromJSON(params, "ID", commId);
	
	m_ptr_rf->setCommID(commId[0]);
}

/**
* @brief Set the packet printing from the Operator application
* @param params JSON string containing the necessary parameters for the operation
*/
void SerialManager::setPacketPrinting(char* params)
{
	char answer[4];
	
	getParamFromJSON(params, "answer", answer);
	
	if(strcmp(answer, "YES") == 0)
		m_ptr_rf->setPrintingPackets(true);
	else if(strcmp(answer, "NO") == 0)
		m_ptr_rf->setPrintingPackets(false);
}

/**
* @brief Manually drop the connexion with a unit from the Operator application
* @param addr Address of the unit to drop form the network
*/
void SerialManager::dropDevice(char* addr)
{
	char packet[128];
	
	initJSON("DROP", packet);
	addParamToJSON("address", addr, packet);
	syst->uart()->sputs(packet);
}

/**
* @brief Manually stop the physical activity of all units on the network from the Operator application
* @param addr Address of the unit to drop form the network
*/
void SerialManager::stopAllDevices(void)
{
	for(uint8_t i = 0; i < m_ptr_rf->unitList()->size(); ++i)
		m_ptr_rf->sendTPOS(i, '9', '0', (char*)"XXX");
	
}

/**
* @brief Send an error message to the Operator in case of misunderstood command
*/
void SerialManager::returnError_OP(void)
{
	char packet[128];
	
	initJSON("ERRO", packet);
	syst->uart()->sputs(packet);
}

/**
* @brief Main operating task of the class
*/
task SerialManager::mainTask(void)
{
	SerialPacket packet;
	
	if(!receivedPacket())
	return;

	packet = extractPacket();
	
	callFunction(packet.func, packet.params);
}

