/**
* @file main.cpp
* @brief Main routine file
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @version 1.0.0
* @date February 11th, 2018
*
* This file is the main routine file. It calls the execution of system tasks.
*
*/

#ifndef F_CPU
/**
* @def F_CPU
* @brief The external clock value of the processor
*/
#define F_CPU 16000000UL
#endif

/**
* @def MS_IN_S
* @brief Number of milliseconds in a second
*/
#define MS_IN_S 1000

#include <avr/io.h>
#include <util/delay.h>

#include "SystemManager.hpp"
#include "RfManager.hpp"
#include "SerialManager.hpp"
#include "ud_std.hpp"

/**
* @brief Current counter in milliseconds
*/
volatile uint16_t timerMsCount;

/**
* @brief Variable buffer for overloaded interrupts
*/
volatile char wasteChar;

/**
* @brief Main instance of the RfManager
*/
RfManager telecom;

/**
* @brief Main instance of the SerialManager
*/
SerialManager serialCom;

/**
* @brief Main routine function
*/
int main(void)
{	
	syst->init();
	
	telecom.setSerial(&serialCom);
	serialCom.setRf(&telecom);
	
    for(;;) 
    {
		serialCom.mainTask();
		telecom.mainTask();
	}
}

/**
* @brief RF module's external interrupt routine
*/
ISR(PCINT0_vect)
{	
	if((PINB & (1 << PINB0)) == 0) //falling edge
		rfModule.intTask();
}

/**
* @brief UART receiving interrupt
*/
ISR(USART_RX_vect)
{
	if(syst->uart()->buffer()->isFull())
		wasteChar = UDR0;
	else
		syst->uart()->intTask();
}

/**
* @brief Main timer interrupt
*/	
ISR(TIMER0_COMPA_vect)
{
	++timerMsCount;
	
	if(timerMsCount == MS_IN_S) // A second has passed
	{
		timerMsCount = 0;
		
		rfModule.timerSecTask();
		
		if(!serialCom.isConnInit())
			serialCom.requestInit();
	}
}

