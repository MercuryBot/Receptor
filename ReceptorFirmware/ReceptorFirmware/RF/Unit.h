/**
* @file Unit.h
* @brief Header file for the Unit struct
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @version 1.0.0
* @date February 16th, 2018
*
* The unit struct describes the basic structure of a connected Unit.
*
*/


#ifndef UNIT_H_
#define UNIT_H_

/**
* @struct Unit
* @brief Structure representing a basic unit's attribute to a network
*/
struct Unit {
	
	/**
	* @brief Address of the unit
	*/
	char address[MAX_ADDR_LEN + 1];
	
	/**
	* @brief Receptor's pipe assigned ot the unit
	*/
	uint8_t assignedPipe;
	
	/**
	* @brief Seconds since the last message
	*/
	uint8_t secSinceLastMsg;
	
	/**
	* @brief Confirms if the Receptor is waiting for a pong response from the unit
	*/
	bool awaitingPong;
	
	/**
	* @brief Seconds since the last ping message sent
	*/
	uint8_t secSincePing;
	
	/**
	* @brief Confirms if the unit is currently executing a sequence
	*/	
	bool sequExecuting;
	
	/**
	* @brief Seconds since the last sequence started
	*/
	uint8_t secSinceSequ;
};


#endif /* UNIT_H_ */