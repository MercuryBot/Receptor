/**
* @file ControlRf.hpp
* @brief Header file for the ControlRf class
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @version 1.0.1
* @date February 16th, 2018
*
* The ControlRf class provides control over the nRF24L01+ module's registers.
*
*/

#ifndef CONTROLRF_HPP_
#define CONTROLRF_HPP_

#include <stdbool.h>
#include <string.h>

#include "ud_std.hpp"
#include "NRF24L01.h"
#include "SystemManager.hpp"
#include "Bits.h"

/**
* @def ENABLE
* @brief Defines enabling value as 1
*/
#define ENABLE 1

/**
* @def DISABLE
* @brief Defines disabling value as 0
*/
#define DISABLE 0

/**
* @def MIN_CHANNEL_FREQ_MHZ
* @brief Minimum channel bandwidth in MHz
*/
#define MIN_CHANNEL_FREQ_MHZ 2400
/**
* @def MAX_CHANNEL_FREQ_MHZ
* @brief Maximum channel bandwidth in MHz
*/
#define MAX_CHANNEL_FREQ_MHZ 2527

/**
* @def PALEVEL1
* @brief First power amplifying level, 0 dBm
*/
#define PALEVEL1 ((0b11)<<1)
/**
* @def PALEVEL2
* @brief Second power amplifying level, -6 dBm
*/
#define PALEVEL2 ((0b10)<<1)
/**
* @def PALEVEL3
* @brief Third power amplifying level, -12 dBm
*/
#define PALEVEL3 ((0b01)<<1)
/**
* @def PALEVEL4
* @brief Fourth power amplifying level, -18 dBm
*/
#define PALEVEL4 ((0b00)<<1)

/**
* @def DATARATE_250KBPS
* @brief Data rate configuration for 250 kB per second
*/
#define DATARATE_250KBPS ((0b100)<<3)
/**
* @def DATARATE_2MBPS
* @brief Data rate configuration for 2 MB per second
*/
#define DATARATE_2MBPS ((0b001)<<3)
/**
* @def DATARATE_1MBPS
* @brief Data rate configuration for 1 MB per second
*/
#define DATARATE_1MBPS ((0b000)<<3)

/**
* @def MAX_DATA_PIPES
* @brief Maximum number of data pipes on module
*/
#define MAX_DATA_PIPES 6
/**
* @def MIN_DATA_PIPES
* @brief Minimum number of data pipes on module
*/
#define MIN_DATA_PIPES 1

/**
* @def MAX_ADDR_LEN
* @brief Maximum address length of a data pipe
*/
#define MAX_ADDR_LEN 5
/**
* @def MIN_ADDR_LEN
* @brief Minimum address length of a data pipe
*/
#define MIN_ADDR_LEN 3

/**
* @def MAX_PL_LEN
* @brief Maximum length of a data packet
*/
#define MAX_PL_LEN 32
/**
* @def MIN_PL_LEN
* @brief Minimum length of a data packet
*/
#define MIN_PL_LEN 1

/**
* @def MAX_RETRY_CNT
* @brief Maximum number or automatic retries after failed packet sending
*/
#define MAX_RETRY_CNT 15
/**
* @def MIN_RETRY_CNT
* @brief Minimum number or automatic retries after failed packet sending
*/
#define MIN_RETRY_CNT 0

/**
* @def MAX_RETRY_DELAY_US
* @brief Maximum retry delay after failed packet sending
*/
#define MAX_RETRY_DELAY_US 4000
/**
* @def MIN_RETRY_DELAY_US
* @brief Minimum retry delay after failed packet sending
*/
#define MIN_RETRY_DELAY_US 250
/**
* @def RETRY_DELAY_STEP_US
* @brief Step between various retry delays after failed packet sending
*/
#define RETRY_DELAY_STEP_US 250

/**
* @def TX_MODE
* @brief Defines transmission mode as value 0
*/
#define TX_MODE 0
/**
* @def RX_MODE
* @brief Defines reception mode as value 1
*/
#define RX_MODE 1

/**
* @def CRC_ENC_2B
* @brief Partial register configuration for two bytes length automatic CRC 
*/
#define CRC_ENC_2B 1
/**
* @def CRC_ENC_1B
* @brief Partial register configuration for one byte length automatic CRC
*/
#define CRC_ENC_1B 0

/**
* @class ControlRf
* @brief Provides control over the nRF24L01+ module's registers and configuration
*/
class ControlRf {

	private:
	/**
	* @brief Power amplifying level
	*/
	uint8_t m_paLevel;
	
	/**
	* @brief Number of active data pipes
	*/
	uint8_t m_dataPipsNum;
	
	/**
	* @brief Configured address length
	*/
	uint8_t m_addrLen;
	
	/**
	* @brief Data rate
	*/
	uint8_t m_dataRate;
	
	/**
	* @brief Packet payload length
	*/
	uint8_t m_payloadLen;
	
	/**
	* @brief Represents the maximum retry count after failed packet sending
	*/
	uint8_t m_retryCount;
	
	/**
	* @brief Represents the current operation mode (0 for transmission, 1 for reception)
	*/
	uint8_t m_opMode;
	
	/**
	* @brief Copy of the CONFIG register
	*/
	uint8_t m_currentConfigMsk;
	
	/**
	* @brief Allowed delay between each re-sending trials
	*/
	uint16_t m_retryDelay;
	
	/**
	* @brief Currently configured bandwidth frequency (MHz)
	*/
	uint16_t m_channelFreq;
	
	/**
	* @brief Auto-acknowledgement enabled (0 for no, 1 for yes)
	*/
	bool m_autoAck;
	
	/**
	* @brief Array with enabling confirmation for each data pipes (0 if disabled, 1 if enabled)
	*/
	bool m_isPipeActive[MAX_DATA_PIPES];
	
	/**
	* @brief Currently configured sending address
	*/
	char m_txAddr[MAX_ADDR_LEN + 1];
	
	/**
	* @brief Array of currently configured reception addresses
	*/
	char m_rxAddrPipe[MAX_DATA_PIPES][MAX_ADDR_LEN + 1];
	
	public:
	
	/**
	* @brief The class' basic constructor
	*/
	ControlRf();
	
	/**
	* @brief The class' basic destructor
	*/
	~ControlRf();
	
	/**
	* @brief Reads a register from the Rf Module
	* @param addr Address of the register
	* @param len Length of the register to read
	* @param a_outdata Output buffer of the read register
	*/
	void readReg(uint16_t addr, size_t len, uint8_t* a_outData);
	
	/**
	* @brief Writes to a register from the Rf Module
	* @param addr Address of the register
	* @param a_data Buffer to write to the register
	* @param len Length of the register to write
	*/
	void writeReg(uint16_t addr, uint8_t* a_data, size_t len);
	
	/**
	* @brief Writes to a register from the Rf Module
	* @param addr Address of the register
	* @param data Single byte to write to the address
	*/
	void writeReg(uint16_t addr, uint8_t data);
	
	/**
	* @brief Reset the RF module for further operations	
	*/
	void resetRF(void);
	
	/**
	* @brief Transmit a message with the currently configured registers
	* @param a_data Buffer to transmit
	* @param len Length of the buffer to transmit
	*/
	void transmitMsg(uint8_t* a_data, size_t len);
	
	/**
	* @brief Receive a message with the currently configured registers
	* @param a_outData Output of read message
	*/
	void receiveMsg(uint8_t* a_outData);
	
	/**
	* @brief Setter for the power amplifying member parameter, with associated registers
	* @param level Value to set the parameter to
	*/
	void setPALevel(uint8_t level);
	
	/**
	* @brief Setter for the auto-acknowledging member parameter, with associated registers
	* @param data Value to set the parameter to (0 for disabling, 1 for enabling)
	*/
	void setAutoAck(bool data);
	
	/**
	* @brief Setter for the number of data pipes, with associated registers
	* @param num Number of data pipes to set
	*/
	void setDataPipsNum(uint8_t num);
	
	/**
	* @brief Setter for the enabling of individual data pipes, with associated registers
	* @param dataPipes Array of data pipes, with each index corresponding to a data pipe enabling or disabling
	*/
	void enableDataPips(bool* dataPipes);
	
	/**
	* @brief Setter for the enabling of individual data pipe, with associated registers
	* @param idx Index of data pipes to enable
	*/
	void enableDataPipe(uint8_t idx);
	
	/**
	* @brief Setter for the disabling of individual data pipe, with associated registers
	* @param idx Index of data pipes to disable
	*/
	void disableDataPipe(uint8_t idx);
	
	/**
	* @brief Setter for the data pipes address length, with associated registers
	* @param len New address length to set
	*/
	void setAddrLen(uint8_t len);
	
	/**
	* @brief Setter for the channel frequency, with associated registers
	* @param freq New channel frequency to set
	*/
	void setChannelFreqMHZ(uint16_t freq);
	
	/**
	* @brief Setter for the data rate, with associated registers
	* @param rate New data rate to set
	*/
	void setDataRate(uint8_t rate);
	
	/**
	* @brief Setter for the payload length, with associated registers
	* @param len New payload length to set
	*/
	void setPayloadLen(uint8_t len);
	
	/**
	* @brief Setter for the allowed retry count after failed sending, with associated registers
	* @param count New retry count to set
	*/
	void setRetryCount(uint8_t count);
	
	/**
	* @brief Setter for the retry delay after failed sending, with associated registers
	* @param delay New retry delay to set
	*/
	void setRetryDelay(uint16_t delay);
	
	/**
	* @brief Setter for the address of a data pipe, with associated registers
	* @param pipe Index of the data pipe to set the new address
	* @param pipeAddr New address to set to pipe
	*/
	void setListeningPipe(uint8_t pipe, char* pipeAddr);
	
	/**
	* @brief Setter for a specific char in the address of a data pipe, with associated registers
	* @param idx Index of the data pipe to set the new address
	* @param car New character value to set
	* @param pos Position of the character to set
	*/
	void setListeningPipeAtPos(uint8_t idx, char car, uint8_t pos);
	
	/**
	* @brief Setter for the sending address, with associated registers
	* @param pipeAddr New address to set
	*/
	void setSendingPipe(char* pipeAddr);
	
	/**
	* @brief Setter for the operation mode of the RF module, with associated registers
	* @param mode Mode to set the RF module to (1 for reception mode, 0 for transmission mode)
	*/
	void setOpMode(bool mode);
	
	/**
	* @brief Setter the power up configuration of the RF module, with associated registers
	* @param mode Mode for the power up (0 for power down, 1 for power up)
	*/
	void setPwr(bool mode);
	
	/**
	* @brief Setter the automatic CRC configuration/byte length, with associated registers
	* @param mode Mode for the CRC (0 for 1 byte long, 1 for 2 bytes long)
	*/
	void setCrcEnc(bool mode);
	
	/**
	* @brief Setter the automatic CRC enabling, with associated registers
	* @param mode Mode for the CRC (0 for disabled, 1 for enabled)
	*/
	void setEnablingCRC(bool mode);
	
	/**
	* @brief Setter for enabling the empty receiving buffer external interrupt, with associated registers
	* @param mode Mode for the interrupt (0 for disabled, 1 for enabled)
	*/
	void setIntRt(bool mode);
	
	/**
	* @brief Setter for enabling the transmission external interrupt, with associated registers
	* @param mode Mode for the interrupt (0 for disabled, 1 for enabled)
	*/
	void setIntTx(bool mode);
	
	/**
	* @brief Setter for enabling the reception external interrupt, with associated registers
	* @param mode Mode for the interrupt (0 for disabled, 1 for enabled)
	*/
	void setIntRx(bool mode);

	/**
	* @brief Setter for the CONFIG register
	* @param config Value to set to the register
	*/	
	void setConfig(uint8_t config);
	
	/**
	* @brief Set the RF module in listening mode
	*/
	void startListening(void);
	
	/**
	* @brief Get the RF module out of listening mode
	*/
	void stopListening(void);
	
	/**
	* @brief Getter for a certain pipe address
	* @param idx Index of the pipe
	* @param outData Output of the read pipe address
	*/
	void getPipAddr(uint8_t idx, char* outData);
	
	/**
	* @brief Getter for the currently set power amplifier level
	* @return Value of the power amplifier configuration
	*/
	uint8_t getPaLevel(void) const;
	
	/**
	* @brief Getter for the currently set number of data pipes
	* @return Number of data pipes
	*/
	uint8_t getDataPipsNum(void) const;
	
	/**
	* @brief Getter for the currently set pipe address length
	* @return Value of the power amplifier configuration
	*/
	uint8_t getAddrLen(void) const;
	
	/**
	* @brief Getter for the currently set data rate
	* @return Value of the data rate configuration
	*/
	uint8_t getDataRate(void) const;
	
	/**
	* @brief Getter for the currently set payload length
	* @return Value of the payload length configuration
	*/
	uint8_t getPayloadLen(void) const;
	
	/**
	* @brief Getter for the currently set retry count
	* @return Value of the retry count configuration
	*/
	uint8_t retryCount(void) const;
	
	/**
	* @brief Getter for the currently set retry delay
	* @return Value of the retry delay configuration
	*/
	uint8_t retryDelay(void) const;
	
	/**
	* @brief Getter for the currently set operation mode
	* @return Operation mode (0 for transmission, 1 for reception)
	*/
	uint8_t opMode(void) const;
	
	/**
	* @brief Getter for the currently set channel frequency
	* @return Value of the channel frequency
	*/
	uint16_t getChannelFreq(void) const;
	
	/**
	* @brief Confirms if auto-acknowledging is enabled
	* @return 0 if auto-acknowledging is disabled, 1 if it's enabled
	*/
	bool isAutoAck(void) const;
	
	/**
	* @brief Confirms if a data pipe is enabled
	* @param idx Index of the datam pipe to check
	* @return 0 if data pipe is disabled, 1 if it's enabled
	*/
	bool isPipeActive(uint8_t idx) const;
	
	/**
	* @brief Prints out internal registers configuration for debugging purposes
	* @param printOut(const char*) Function to print the information to
	*/
	void outDetails(void (*printOut) (const char*));
};

#endif // CONTROLRF_HPP_