/**
* @file ControlRf.cpp
* @brief Definition file for the ControlRf class
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @version 1.0.1
* @date February 16th, 2018
*
* The ControlRf class provides control over the nRF24L01+ module's registers.
*
*/

#include "ControlRf.hpp"

/**
* @brief The class' basic constructor
*/
ControlRf::ControlRf()
{
	m_paLevel = 0;
	m_dataPipsNum = 0;
	m_addrLen = 0;
	m_dataRate = 0;
	m_payloadLen = 0;
	m_retryCount = 0;
	m_retryDelay = 0;
	m_opMode = 0;
	m_currentConfigMsk = 0;
	
	m_channelFreq = MIN_CHANNEL_FREQ_MHZ;
	
	m_autoAck = 0;
	
	memset(m_txAddr, 0, MAX_ADDR_LEN);
	
	for(uint8_t i = 0; i < MAX_DATA_PIPES; ++i)
	{
		memset(m_rxAddrPipe[i], 0, MAX_ADDR_LEN + 1);
		m_isPipeActive[i] = false;
	}
}

/**
* @brief The class' basic destructor
*/
ControlRf::~ControlRf()
{
	
}

/**
* @brief Reads a register from the Rf Module
* @param addr Address of the register
* @param len Length of the register to read
* @param a_outdata Output buffer of the read register
*/
void ControlRf::readReg(uint16_t addr, size_t len, uint8_t* a_outData)
{
	_delay_us(10);
	syst->spi()->toggleCSNPin(0);
	_delay_us(10);
	
	syst->spi()->exchByte(R_REGISTER + addr);
	
	_delay_us(10);
	
	for(uint8_t i = 0; i < len; ++i)
	{
		a_outData[i] = syst->spi()->exchByte(RF24_NOP);
		_delay_us(10);
	}
	
	syst->spi()->toggleCSNPin(1);
}

/**
* @brief Writes to a register from the Rf Module
* @param addr Address of the register
* @param a_data Buffer to write to the register
* @param len Length of the register to write
*/
void ControlRf::writeReg(uint16_t addr, uint8_t* a_data, size_t len)
{
	_delay_us(10);
	syst->spi()->toggleCSNPin(0);
	_delay_us(10);
	
	if(addr == FLUSH_TX || addr == W_TX_PAYLOAD) //TODO: Check if there's other cases like this
		syst->spi()->exchByte(R_REGISTER + addr);
	else
		syst->spi()->exchByte(W_REGISTER + addr);
	
	_delay_us(10);
	
	for(uint8_t i = 0; i < len; ++i)
	{
		syst->spi()->exchByte(a_data[i]);
		_delay_us(10);
	}
	
	syst->spi()->toggleCSNPin(1);
}

/**
* @brief Writes to a register from the Rf Module
* @param addr Address of the register
* @param data Single byte to write to the address
*/
void ControlRf::writeReg(uint16_t addr, uint8_t data)
{
	_delay_us(10);
	syst->spi()->toggleCSNPin(0);
	_delay_us(10);
	
	if(addr == FLUSH_TX || addr == W_TX_PAYLOAD) //TODO: Check if there's other cases like this
	syst->spi()->exchByte(R_REGISTER + addr);
	else
	syst->spi()->exchByte(W_REGISTER + addr);
	
	_delay_us(10);
	syst->spi()->exchByte(data);
	_delay_us(10);
	
	syst->spi()->toggleCSNPin(1);
}

/**
* @brief Reset the RF module for further operations	
*/
void ControlRf::resetRF()
{
	_delay_us(10);
	syst->spi()->toggleCSNPin(0);
	_delay_us(10);
	writeReg(NRF_STATUS, 0x70);
	_delay_us(20);
	syst->spi()->toggleCSNPin(1);
}

/**
* @brief Transmit a message with the currently configured registers
* @param a_data Buffer to transmit
* @param len Length of the buffer to transmit
*/
void ControlRf::transmitMsg(uint8_t* a_data, size_t len)
{
	writeReg(FLUSH_TX, a_data, 0);
	writeReg(W_TX_PAYLOAD, a_data, len);
	
	_delay_ms(10);
	syst->spi()->toggleCEPin(1);
	_delay_us(20);
	syst->spi()->toggleCEPin(0);
	_delay_ms(10);
	
	resetRF();
}

/**
* @brief Receive a message with the currently configured registers
* @param a_outData Output of read message
*/
void ControlRf::receiveMsg(uint8_t* a_outData)
{
	readReg(R_RX_PAYLOAD, m_payloadLen, a_outData);
}

/**
* @brief Setter for the power amplifying member parameter, with associated registers
* @param level Value to set the parameter to
*/
void ControlRf::setPALevel(uint8_t level)
{
	if(level != PALEVEL1 && level != PALEVEL2 && level != PALEVEL3 && level != PALEVEL4)
	return;
	
	m_paLevel = level;
	
	writeReg(RF_SETUP, (m_paLevel | m_dataRate));
}

/**
* @brief Setter for the auto-acknowledging member parameter, with associated registers
* @param data Value to set the parameter to (0 for disabling, 1 for enabling)
*/
void ControlRf::setAutoAck(bool data)
{
	m_autoAck = data;
	
	if(data)
		writeReg(EN_AA, 0x3F);
	else
		writeReg(EN_AA, 0x00);
}

/**
* @brief Setter for the number of data pipes, with associated registers
* @param num Number of data pipes to set
*/
void ControlRf::setDataPipsNum(uint8_t num)
{
	m_dataPipsNum = ud_std::clamp(num, MIN_DATA_PIPES, MAX_DATA_PIPES);
}

/**
* @brief Setter for the data pipes address length, with associated registers
* @param len New address length to set
*/
void ControlRf::setAddrLen(uint8_t len)
{
	m_addrLen = ud_std::clamp(len, MIN_ADDR_LEN, MAX_ADDR_LEN);

	writeReg(SETUP_AW, (m_addrLen - 2));
}

/**
* @brief Setter for the channel frequency, with associated registers
* @param freq New channel frequency to set
*/
void ControlRf::setChannelFreqMHZ(uint16_t freq)
{
	m_channelFreq = ud_std::clamp(freq, MIN_CHANNEL_FREQ_MHZ, MAX_CHANNEL_FREQ_MHZ);
	
	writeReg(RF_CH, (m_channelFreq - MIN_CHANNEL_FREQ_MHZ));
}

/**
* @brief Setter for the data rate, with associated registers
* @param rate New data rate to set
*/
void ControlRf::setDataRate(uint8_t rate)
{
	if(rate != DATARATE_250KBPS && rate != DATARATE_1MBPS && rate != DATARATE_2MBPS)
	return;
	
	m_dataRate = rate;
	
	writeReg(RF_SETUP, (m_paLevel | m_dataRate));
}

/**
* @brief Setter for the payload length, with associated registers
* @param len New payload length to set
*/
void ControlRf::setPayloadLen(uint8_t len)
{
	m_payloadLen = ud_std::clamp(len, MIN_PL_LEN, MAX_PL_LEN);
	
	writeReg(RX_PW_P0, m_payloadLen);
	writeReg(RX_PW_P1, m_payloadLen);
	writeReg(RX_PW_P2, m_payloadLen);
	writeReg(RX_PW_P3, m_payloadLen);
	writeReg(RX_PW_P4, m_payloadLen);
	writeReg(RX_PW_P5, m_payloadLen);
}

/**
* @brief Setter for the allowed retry count after failed sending, with associated registers
* @param count New retry count to set
*/
void ControlRf::setRetryCount(uint8_t count)
{
	m_retryCount = ud_std::clamp(count, MIN_RETRY_CNT, MAX_RETRY_CNT);

	writeReg(SETUP_RETR, (((uint8_t)((m_retryDelay  - MIN_RETRY_DELAY_US)/RETRY_DELAY_STEP_US)<<4) | m_retryCount));
}

/**
* @brief Setter for the retry delay after failed sending, with associated registers
* @param delay New retry delay to set
*/
void ControlRf::setRetryDelay(uint16_t delay)
{
	m_retryDelay = ud_std::clamp(delay, MIN_RETRY_DELAY_US, MAX_RETRY_DELAY_US);
	
	writeReg(SETUP_RETR, (((uint8_t)((m_retryDelay  - MIN_RETRY_DELAY_US)/RETRY_DELAY_STEP_US)<<4) | m_retryCount));
}

/**
* @brief Setter for the enabling of individual data pipes, with associated registers
* @param dataPipes Array of data pipes, with each index corresponding to a data pipe enabling or disabling
*/
void ControlRf::enableDataPips(bool* dataPipes)
{
	uint8_t tmpReg = 0;

	for(uint8_t i = 0; i < MAX_DATA_PIPES; ++i)
	{
		m_isPipeActive[i] = dataPipes[i];
		tmpReg 	|= dataPipes[i]<<(ERX_P0 + i); 
	}
	
	writeReg(EN_RXADDR, tmpReg);
}

/**
* @brief Setter for the enabling of individual data pipe, with associated registers
* @param idx Index of data pipes to enable
*/
void ControlRf::enableDataPipe(uint8_t idx)
{
	uint8_t tmpReg = 0;
	
	idx = ud_std::clamp(idx, 0, (MAX_DATA_PIPES - 1));
	m_isPipeActive[idx] = true;
	
	for(uint8_t i = 0; i < MAX_DATA_PIPES; ++i)
		tmpReg |= m_isPipeActive[i]<<(ERX_P0 + i);
		
	writeReg(EN_RXADDR, tmpReg);
}

/**
* @brief Setter for the disabling of individual data pipe, with associated registers
* @param idx Index of data pipes to disable
*/
void ControlRf::disableDataPipe(uint8_t idx)
{
	uint8_t tmpReg = 0;
	
	idx = ud_std::clamp(idx, 0, (MAX_DATA_PIPES - 1));
	m_isPipeActive[idx] = false;
	
	for(uint8_t i = 0; i < MAX_DATA_PIPES; ++i)
		tmpReg |= m_isPipeActive[i]<<(ERX_P0 + i);
	
	writeReg(EN_RXADDR, tmpReg);
}

/**
* @brief Setter for the address of a data pipe, with associated registers
* @param pipe Index of the data pipe to set the new address
* @param pipeAddr New address to set to pipe
*/
void ControlRf::setListeningPipe(uint8_t idx, char* pipeAddr)
{
	idx = ud_std::clamp(idx, 0, (MAX_DATA_PIPES - 1));
	
	writeReg((RX_ADDR_P0 + idx), (uint8_t*)pipeAddr, m_addrLen);
	
	strcpy(m_rxAddrPipe[idx], pipeAddr);
}

/**
* @brief Setter for a specific char in the address of a data pipe, with associated registers
* @param idx Index of the data pipe to set the new address
* @param car New character value to set
* @param pos Position of the character to set
*/
void ControlRf::setListeningPipeAtPos(uint8_t idx, char car, uint8_t pos)
{
	char pipeAddr[MAX_ADDR_LEN + 1] = {0};
	
	idx = ud_std::clamp(idx, 0, (MAX_DATA_PIPES - 1));
	pos = ud_std::clamp(pos, 0, (m_addrLen - 1));
	
	readReg((RX_ADDR_P0 + idx), MAX_ADDR_LEN, (uint8_t*)pipeAddr);
	pipeAddr[pos] = car;
	strcpy(m_rxAddrPipe[idx], pipeAddr);
	writeReg((RX_ADDR_P0 + idx), (uint8_t*)pipeAddr, m_addrLen);
}

/**
* @brief Setter for the sending address, with associated registers
* @param pipeAddr New address to set
*/
void ControlRf::setSendingPipe(char* pipeAddr)
{
	memcpy(m_txAddr, pipeAddr, m_addrLen);
	writeReg(TX_ADDR, (uint8_t*)pipeAddr, m_addrLen);
}

/**
* @brief Setter for the operation mode of the RF module, with associated registers
* @param mode Mode to set the RF module to (1 for reception mode, 0 for transmission mode)
*/
void ControlRf::setOpMode(bool mode)
{
	if(mode) //RX
	{
		syst->spi()->toggleCEPin(1); //stop listening
		m_currentConfigMsk |= (1<<PRIM_RX);
	}
	else
	{
		syst->spi()->toggleCEPin(0); //stop listening
		m_currentConfigMsk &= ~(1<<PRIM_RX);
	}
	writeReg(NRF_CONFIG, &m_currentConfigMsk, 1);
}

/**
* @brief Setter the power up configuration of the RF module, with associated registers
* @param mode Mode for the power up (0 for power down, 1 for power up)
*/
void ControlRf::setPwr(bool mode)
{
	if(mode)
		m_currentConfigMsk |= (1<<PWR_UP);
	else
		m_currentConfigMsk &= ~(1<<PWR_UP);
	
	writeReg(NRF_CONFIG, &m_currentConfigMsk, 1);
}

/**
* @brief Setter the automatic CRC configuration/byte length, with associated registers
* @param mode Mode for the CRC (0 for 1 byte long, 1 for 2 bytes long)
*/
void ControlRf::setCrcEnc(bool mode)
{
	if(mode)
		m_currentConfigMsk |= (1<<CRCO);
	else
		m_currentConfigMsk &= ~(1<<CRCO);
	
	writeReg(NRF_CONFIG, &m_currentConfigMsk, 1);
}

/**
* @brief Setter the automatic CRC enabling, with associated registers
* @param mode Mode for the CRC (0 for disabled, 1 for enabled)
*/
void ControlRf::setEnablingCRC(bool mode)
{
	if(mode)
	m_currentConfigMsk |= (1<<EN_CRC);
	else
	m_currentConfigMsk &= ~(1<<EN_CRC);
	
	writeReg(NRF_CONFIG, &m_currentConfigMsk, 1);
}

/**
* @brief Setter for enabling the empty receiving buffer external interrupt, with associated registers
* @param mode Mode for the interrupt (0 for disabled, 1 for enabled)
*/
void ControlRf::setIntRt(bool mode)
{
	if(mode)
		m_currentConfigMsk |= (1<<MASK_MAX_RT);
	else
		m_currentConfigMsk &= ~(1<<MASK_MAX_RT);
	
	writeReg(NRF_CONFIG, &m_currentConfigMsk, 1);
}

/**
* @brief Setter for enabling the transmission external interrupt, with associated registers
* @param mode Mode for the interrupt (0 for disabled, 1 for enabled)
*/
void ControlRf::setIntTx(bool mode)
{
	if(mode)
		m_currentConfigMsk |= (1<<MASK_TX_DS);
	else
		m_currentConfigMsk &= ~(1<<MASK_TX_DS);
	
	writeReg(NRF_CONFIG, &m_currentConfigMsk, 1);
}

/**
* @brief Setter for enabling the reception external interrupt, with associated registers
* @param mode Mode for the interrupt (0 for disabled, 1 for enabled)
*/
void ControlRf::setIntRx(bool mode)
{
	if(mode)
	m_currentConfigMsk |= (1<<MASK_RX_DR);
	else
	m_currentConfigMsk &= ~(1<<MASK_RX_DR);
	
	writeReg(NRF_CONFIG, &m_currentConfigMsk, 1);
}

/**
* @brief Setter for the CONFIG register
* @param config Value to set to the register
*/
void ControlRf::setConfig(uint8_t config)
{
	m_currentConfigMsk = config;
	
	writeReg(NRF_CONFIG, &m_currentConfigMsk, 1);
}

/**
* @brief Set the RF module in listening mode
*/
void ControlRf::startListening(void)
{
	syst->spi()->toggleCEPin(1);
	_delay_us(10);
}

/**
* @brief Get the RF module out of listening mode
*/
void ControlRf::stopListening(void)
{
	syst->spi()->toggleCEPin(0);
	_delay_us(10);
}

/**
* @brief Getter for a certain pipe address
* @param idx Index of the pipe
* @param outData Output of the read pipe address
*/
void ControlRf::getPipAddr(uint8_t idx, char* outData)
{
	memcpy(outData, m_rxAddrPipe[idx], m_addrLen);
}

/**
* @brief Getter for the currently set power amplifier level
* @return Value of the power amplifier configuration
*/
uint8_t ControlRf::getPaLevel(void) const
{
	return m_paLevel;
}

/**
* @brief Getter for the currently set number of data pipes
* @return Number of data pipes
*/
uint8_t ControlRf::getDataPipsNum(void) const
{
	return m_dataPipsNum;
}

/**
* @brief Getter for the currently set pipe address length
* @return Value of the power amplifier configuration
*/
uint8_t ControlRf::getAddrLen(void) const
{
	return m_addrLen;
}

/**
* @brief Getter for the currently set data rate
* @return Value of the data rate configuration
*/
uint8_t ControlRf::getDataRate(void) const
{
	return m_dataRate;
}

/**
* @brief Getter for the currently set payload length
* @return Value of the payload length configuration
*/
uint8_t ControlRf::getPayloadLen(void) const
{
	return m_payloadLen;
}

/**
* @brief Getter for the currently set retry count
* @return Value of the retry count configuration
*/
uint8_t ControlRf::retryCount(void) const
{
	return m_retryCount;
}

/**
* @brief Getter for the currently set retry delay
* @return Value of the retry delay configuration
*/
uint8_t ControlRf::retryDelay(void) const
{
	return m_retryDelay;
}

/**
* @brief Getter for the currently set operation mode
* @return Operation mode (0 for transmission, 1 for reception)
*/
uint8_t ControlRf::opMode(void) const
{
	return m_opMode;
}

/**
* @brief Getter for the currently set channel frequency
* @return Value of the channel frequency
*/
uint16_t ControlRf::getChannelFreq(void) const
{
	return m_channelFreq;
}

/**
* @brief Confirms if auto-acknowledging is enabled
* @return 0 if auto-acknowledging is disabled, 1 if it's enabled
*/
bool ControlRf::isAutoAck(void) const
{
	return m_autoAck;
}

/**
* @brief Confirms if a data pipe is enabled
* @param idx Index of the datam pipe to check
* @return 0 if data pipe is disabled, 1 if it's enabled
*/
bool ControlRf::isPipeActive(uint8_t idx) const
{
	if(idx > MAX_DATA_PIPES - 1)
	return false;
	
	return m_isPipeActive[idx];
}

/**
* @brief Prints out internal registers configuration for debugging purposes
* @param printOut(const char*) Function to print the information to
*/
void ControlRf::outDetails(void (*printOut) (const char*))
{
	uint8_t readRegTmp[5];
	char printStr[32];
	
	readReg(NRF_STATUS, 1, readRegTmp);
	sprintf(printStr, "NRF_STATUS: 0x%02x \r\n", readRegTmp[0]);
	printOut(printStr);
	
	readReg(NRF_CONFIG, 1, readRegTmp);
	sprintf(printStr, "NRF_CONFIG: 0x%02x \r\n", readRegTmp[0]);
	printOut(printStr);
	
	readReg(RX_ADDR_P0, 5, readRegTmp);
	sprintf(printStr, "RX_ADDR_P0: 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x \r\n", readRegTmp[0], readRegTmp[1], readRegTmp[2], readRegTmp[3], readRegTmp[4]);
	printOut(printStr);

	readReg(RX_PW_P0, 1, readRegTmp);
	sprintf(printStr, "RX_PW_P0: 0x%02x  \r\n", readRegTmp[0]);
	printOut(printStr);

	readReg(TX_ADDR, 5, readRegTmp);
	sprintf(printStr, "TX_ADDR: 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x \r\n", readRegTmp[0], readRegTmp[1], readRegTmp[2], readRegTmp[3], readRegTmp[4]);
	printOut(printStr);
	
	readReg(EN_AA, 1, readRegTmp);
	sprintf(printStr, "EN_AA: 0x%02x \r\n", readRegTmp[0]);
	printOut(printStr);
	
	readReg(SETUP_AW, 1, readRegTmp);
	sprintf(printStr, "SETUP_AW: 0x%02x \r\n", readRegTmp[0]);
	printOut(printStr);
	
	readReg(SETUP_RETR, 1, readRegTmp);
	sprintf(printStr, "SETUP_RETR: 0x%02x \r\n", readRegTmp[0]);
	printOut(printStr);
	
	readReg(EN_RXADDR, 1, readRegTmp);
	sprintf(printStr, "EN_RXADDR: 0x%02x \r\n", readRegTmp[0]);
	printOut(printStr);
	
	readReg(RF_CH, 1, readRegTmp);
	sprintf(printStr, "RF_CH: 0x%02x \r\n", readRegTmp[0]);
	printOut(printStr);
	
	readReg(RF_SETUP, 1, readRegTmp);
	sprintf(printStr, "RF_SETUP: 0x%02x \r\n", readRegTmp[0]);
	printOut(printStr);
	
	readReg(DYNPD, 1, readRegTmp);
	sprintf(printStr, "DYNPD: 0x%02x \r\n", readRegTmp[0]);
	printOut(printStr);
	
	readReg(FEATURE, 1, readRegTmp);
	sprintf(printStr, "FEATURE: 0x%02x \r\n", readRegTmp[0]);
	printOut(printStr);
}