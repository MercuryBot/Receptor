/**
* @file RfManager.cpp
* @brief Definition file for the RfManager class
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @version 1.0.0
* @date February 17th, 2018
*
* The RfManager class provides control over the nRF24L01+ module
* and the MercuryBot communication protocol.
*
*/

#include "RfManager.hpp"
#include "SerialManager.hpp"

/**
* @brief The class' base constructor
*/
RfManager::RfManager()
{
	ControlRf();
	
	m_unitList = LinkedList<Unit>();
	m_buffer= RingBuffer<char>(128);
	
	m_init = false;
	m_printPackets = false;
	memset(m_deviceNumOnPipe, 0, sizeof(m_deviceNumOnPipe));
	m_commId = '*';
	m_commDeviceType = DEVICE_TYPE_NOT_INIT;
}

/**
* @brief The class' base destructor
*/
RfManager::~RfManager()
{
	
}

/**
* @brief Getter of the pointer to the receiving buffer
* @return Pointer to the receiving buffer
*/
RingBuffer<char>* RfManager::buffer(void)
{
	return &m_buffer;
}

/**
* @brief Send data to a specific address 
* @param a_addr Address to send the data to
* @param a_data Data to send over
*/
void RfManager::sendData(char* a_addr, char* a_data)
{
	setSendingPipe(a_addr);
	setOpMode(0); //TX mode
	transmitMsg((uint8_t*)a_data, getPayloadLen());
	setSendingPipe((char*)"00000");
	setOpMode(1);
	
	if(m_printPackets)
		m_ptr_serial->getMonitoredPacket(a_data, (char*)"OUT");
}

/**
* @brief Reset and disable a listening pipe
* @param idx Index of the targeted pipe
*/
void RfManager::dropPipe(uint8_t idx)
{
	if(!isPipeActive(idx)) //Pipe already dropped
		return;
		
	setDataPipsNum(getDataPipsNum() - 1);	
	setListeningPipe(idx, (char*)"00000");
	disableDataPipe(idx);
}

/**
* @brief Enable and set a pipe's specifics
* @param idx Index of the targeted pipe
* @param a_addr New address to set
*/
void RfManager::setPipe(uint8_t idx, char* a_addr)
{
	if(!isPipeActive(idx))
		setDataPipsNum(getDataPipsNum() + 1);
	
	setListeningPipe(idx, a_addr);
	enableDataPipe(idx);
}

/**
* @brief Setter for the packet printing confirmation
* @param data 0 to disable packet printing over serial port, 1 to enable it
*/
void RfManager::setPrintingPackets(bool data)
{
	m_printPackets = data;
}

/**
* @brief Getter of the total number of connected units to the Receptor
* @return The total number of connected units to the Receptor
*/
uint8_t RfManager::getDeviceNum(void) const
{
	return (m_unitList.size());
}

/**
* @brief Getter of the number of connected units one of the Receptor's pipes
* @param idx Index of the targeted pipe
* @return The number of connected units to the Receptor's pipe
*/
uint8_t RfManager::getDeviceNumOnPipe(uint8_t idx) const
{
	return (m_deviceNumOnPipe[idx]);
}
	
/**
* @brief Setter for the communication ID of the Receptor, with corresponding registers transformation
* @param ID New communication ID to set  
*/
void RfManager::setCommID(char commId)
{	
	if(!(commId >= '0' && commId <= 'Z'))
		return;
	
	m_commId = commId;
	
	for(uint8_t i = 0; i < 6; ++i)
	{
		setListeningPipeAtPos(i, m_commId, ADDR_COMM_ID_IDX);
	}
}

/**
* @brief Getter of the communication ID of the Receptor
* @return The communication ID of the Receptor
*/
char RfManager::getCommID(void) const
{
	return (m_commId);
}

/**
* @brief Confirms if the RF module has been correctly initialized
* @return 0 if it hasn't been initialized, 1 if it has
*/
bool RfManager::isInit(void) const
{
	return m_init;
}

/**
* @brief Getter to a pointer to the units list
* @return The pointer to the units list
*/
LinkedList<Unit>* RfManager::unitList(void)
{
	return &m_unitList;
}

/**
* @brief Generates an address in the form of a string based on the passed parameters 
* @param index Index of the pipe
* @param func Function of the pipe
* @param commId Communication ID of the device
* @param deviceType Type of the device
* @param status Status of the pipe
* @param a_outData Output of the generated address
*/
void RfManager::genReplyAddr(char index, char func, char commId, char deviceType, char status, char* a_outData)
{
	a_outData[ADDR_PIPE_IDX] = index;
	a_outData[ADDR_FUNC_IDX] = func;
	a_outData[ADDR_COMM_ID_IDX] = commId;
	a_outData[ADDR_DEVICE_TYPE_IDX] = deviceType;
	a_outData[ADDR_STATUS_IDX] = status;
}

/**
* @brief Initializes the RfModule to fit the telecommunication protocol
*/
void RfManager::init(void)
{
	uint8_t tmpReg;
	uint8_t tmp[32];
	bool tmpDataPipes[MAX_DATA_PIPES] = {true, true, true, true, true, true};
		
	if(!syst->isInit())
		return;
	
	_delay_ms(100);	//Leave time if in shutdown process
	
	setAutoAck(true);
	setDataPipsNum(MAX_DATA_PIPES);
	enableDataPips(tmpDataPipes);
	setAddrLen(MAX_ADDR_LEN);
	setChannelFreqMHZ(OPERATING_FREQUENCY_MHZ);
	setDataRate(DATARATE_1MBPS);
	setPALevel(PALEVEL4);

	setListeningPipe(0, (char*)"020R0");
	setListeningPipe(1, (char*)"110R0");
	setListeningPipe(2, (char*)"210R0");
	setListeningPipe(3, (char*)"310R0");
	setListeningPipe(4, (char*)"410R0");
	setListeningPipe(5, (char*)"510R0");
	
	setSendingPipe((char*)"00000");
	
	setPayloadLen(MAX_PL_LEN);
	
	setRetryCount(MAX_RETRY_CNT);
	setRetryDelay(MAX_RETRY_DELAY_US);
	
	tmpReg = RX_CONFIG;
	setConfig(tmpReg); //Register is set directly to save writing time (also logically simpler)
	
	readReg(R_RX_PAYLOAD, 32, tmp); //Line to avoid undetected rx interrupt bug.
	resetRF();

	memset(m_deviceNumOnPipe, 0, sizeof(m_deviceNumOnPipe));
	m_commDeviceType = DEVICE_TYPE_RECEPT;
	
	_delay_ms(100);
	
	syst->spi()->toggleCEPin(1); //Start listening
	
	m_init = true;
}

/**
* @brief Main task to execute
*/
task RfManager::mainTask(void)
{
	RfPacket inPacket;
		
	if(!m_ptr_serial->isConnInit())
		return;
	
	if(!isInit())
		init();
	
	if(receivedPacket()) 
	{	
		inPacket = extractPacket();
		receptFunction(inPacket);
	}
	
	for(uint8_t i = 0; i < m_unitList.size(); ++i)
	{
		if(m_unitList.at(i).awaitingPong && m_unitList.at(i).secSincePing > MAX_PONG_DELAY_SEC) //Device is dead
		{
			//Drop device
			--m_deviceNumOnPipe[m_unitList.at(i).assignedPipe];
			m_unitList.dropAt(i);
		}
		
		if(!m_unitList.at(i).awaitingPong && ((m_unitList.at(i).sequExecuting && m_unitList.at(i).secSinceSequ > MAX_SEQU_DELAY_SEC) ||
												(m_unitList.at(i).secSinceLastMsg > MAX_INTER_DELAY_SEC)))
		{
			//Ping device
			sendPING(m_unitList[i].address);
			m_unitList.at(i).awaitingPong = true;	
		}
	}
}

/**
* @brief Secondary task to execute when the external interrupt is triggered
*/
void RfManager::intTask(void)
{	
	char tmpBuf[PACKET_LEN];
	
	if(!m_init)
		return;
		
	syst->spi()->toggleCEPin(0); //stop listening		
	
	receiveMsg((uint8_t*)tmpBuf);
	m_buffer.appendData(tmpBuf, PACKET_LEN);
	resetRF();
	
	syst->spi()->toggleCEPin(1); //Start listening again
}

/**
* @brief Secondary task to execute periodically
*/
void RfManager::timerSecTask(void)
{
	if(!m_init)
		return;
	
	for(uint8_t i = 0; i < m_unitList.size(); ++i)
	{
		if(m_unitList.at(i).awaitingPong)
			++m_unitList.at(i).secSincePing;
		
		if(m_unitList.at(i).sequExecuting)
			++m_unitList.at(i).secSinceSequ;
			
		++m_unitList.at(i).secSinceLastMsg;
	}
}

/**
* @brief Gets the number of pipes that aren't overloaded yet
* @return Number of pipes that aren't overloaded yet
*/
uint8_t RfManager::getFreePortsQty(void)
{
	return (MAX_DATA_PIPES - getDataPipsNum());
}

/**
* @brief Confirms if a packet has been received and is waiting to be read
* @return 0 if no packet has been received, 1 if a packet has been received and is waiting reading
*/
bool RfManager::receivedPacket(void)
{
	return (m_buffer.contains((char*)"<", 1) && m_buffer.contains((char*)">", 1));
}

/**
* @brief Extracts a packet from the internal registers and casts it into an RfPacket structure
* @return Read and formatted packet
*/
RfPacket RfManager::extractPacket()
{
	char packetBuffer[PACKET_LEN];
	RfPacket returnPacket;
	
	m_buffer.getData(packetBuffer, PACKET_LEN);
	m_buffer.lopData(PACKET_LEN);
	
	if(m_printPackets)
		m_ptr_serial->getMonitoredPacket(packetBuffer, (char*)"IN");
	
	memcpy(returnPacket.senderAddr, &packetBuffer[PACKET_SENDER_ADDR_IDX], MAX_ADDR_LEN);
	returnPacket.senderAddr[MAX_ADDR_LEN] = 0; //Adding null-termination
	memcpy(returnPacket.targetAddr, &packetBuffer[PACKET_TARGET_ADDR_IDX], MAX_ADDR_LEN);
	returnPacket.targetAddr[MAX_ADDR_LEN] = 0; //Adding null-termination
	memcpy(returnPacket.instructHead, &packetBuffer[PACKET_HEAD_IDX], HEAD_LEN);
	returnPacket.instructHead[HEAD_LEN] = 0; //Adding null-termination
	memcpy(returnPacket.instructBody, &packetBuffer[PACKET_BODY_IDX], BODY_LEN);
	returnPacket.instructBody[BODY_LEN] = 0; //Adding null-termination

	if(m_printPackets)
		m_ptr_serial->getMonitoredPacket(packetBuffer, (char*)"IN");
	
	return returnPacket;
}

/**
* @brief Sends a packet on the network
* @param Packet Packet to send
*/
void RfManager::sendPacket(RfPacket packet)
{
	char packetStr[PACKET_LEN];
	
	memset(packetStr, 0, PACKET_LEN);
	
	packetStr[0] = '<';
	memcpy(packetStr + PACKET_SENDER_ADDR_IDX, packet.senderAddr, MAX_ADDR_LEN);
	memcpy(packetStr + PACKET_TARGET_ADDR_IDX, packet.targetAddr, MAX_ADDR_LEN);
	memcpy(packetStr + PACKET_HEAD_IDX, packet.instructHead, HEAD_LEN);
	memcpy(packetStr + PACKET_BODY_IDX, packet.instructBody, BODY_LEN);
	packetStr[PACKET_LEN - 1] = '>';
	
	sendData(packet.targetAddr, packetStr);
}

/**
* @brief Execute the reception and reaction to a ping message
* @param packet Packet to analyze
*/
void RfManager::receptPING(RfPacket packet)
{
	char replyAddress[getAddrLen() + 1] = "";
	
	for(uint8_t i = 0; i < m_unitList.size(); ++i)
		if(strncmp(m_unitList[i].address, packet.instructBody, getAddrLen()) == 0)
			memcpy(replyAddress, m_unitList[i].address, (getAddrLen() + 1));
		
	if(replyAddress[0] == 0)
		memcpy(replyAddress, packet.instructBody, getAddrLen());
		
	sendPONG(replyAddress);
}

/**
* @brief Execute the reception and reaction to a pong (reply to ping) message
* @param packet Packet to analyze
*/
void RfManager::receptPONG(RfPacket packet)
{
	 for(uint8_t i = 0; i < m_unitList.size(); ++i)
	 {
		 if(strncmp(m_unitList[i].address, packet.instructBody, getAddrLen()) == 0)
		{
			m_unitList[i].awaitingPong = false;
			m_unitList[i].secSincePing = 0;
		}
	 }
}

/**
* @brief Execute the reception and reaction to a new connection request message
* @param packet Packet to analyze
*/
void RfManager::receptNCON(RfPacket packet)
{
	if(m_unitList.size() > MAX_UNITS)
	{
		sendBCON(packet.senderAddr);
		return;
	}
	
	for(uint8_t i = 0; i < MAX_DATA_PIPES; ++i)
	{
		if(m_deviceNumOnPipe[i] < MAX_DATA_PIPES)
		{
			sendSCON(packet.senderAddr, i);
			break;
		}
	}	
}

/**
* @brief Execute the reception and reaction to a position update message
* @param packet Packet to analyze
*/
void RfManager::receptCPOS(RfPacket packet)
{	
	uint8_t code[3];
	memcpy(code, (uint8_t*)packet.instructBody, 3);
	
	m_ptr_serial->recvCPOS(packet.senderAddr, code);
}

/**
* @brief Execute the reception and reaction to a low battery alert message
* @param packet Packet to analyze
*/
void RfManager::receptABAT(RfPacket packet)
{
	m_ptr_serial->recvABAT(packet.senderAddr);
}

/**
* @brief Execute the reception and reaction to a battery level message
* @param packet Packet to analyze
*/
void RfManager::receptLBAT(RfPacket packet)
{
	uint32_t level = (uint32_t)packet.instructBody[0]<<24 | (uint32_t)packet.instructBody[1]<<16 | (uint32_t)packet.instructBody[2]<<8 | (uint32_t)packet.instructBody[3];
	m_ptr_serial->recvLBAT(packet.senderAddr, level);
}

/**
* @brief Execute the reception and reaction to a state alert message
* @param packet Packet to analyze
*/
void RfManager::receptALRT(RfPacket packet)
{
	m_ptr_serial->recvALRT(packet.senderAddr, packet.instructBody[0]);
}

/**
* @brief Execute the reception and reaction to a current state update message
* @param packet Packet to analyze
*/
void RfManager::receptCSTT(RfPacket packet)
{
	m_ptr_serial->recvCSTT(packet.senderAddr, (uint8_t*)packet.instructBody);
}

/**
* @brief Execute the reception and reaction to a done sequence message
* @param packet Packet to analyze
*/
void RfManager::receptSDON(RfPacket packet)
{
	m_ptr_serial->recvSDON(packet.senderAddr, packet.instructBody[0]);
	
	for(uint8_t i = 0; i < m_unitList.size(); ++i)
	{
		if(strncmp(packet.senderAddr, m_unitList[i].address, getAddrLen()) == 0)
		{
			m_unitList[i].secSinceLastMsg = 0;
			m_unitList[i].sequExecuting = false;
			m_unitList[i].secSinceSequ = 0;	
			break;
		}
	}
}

/**
* @brief Execute the reception and reaction to a dropped connexion message
* @param packet Packet to analyze
*/
void RfManager::receptDROP(RfPacket packet)
{
	for(uint8_t i = 0; i < m_unitList.size(); ++i)
	{
		if(strncmp(packet.senderAddr, m_unitList[i].address, getAddrLen()) == 0)
		{
			--m_deviceNumOnPipe[m_unitList[i].assignedPipe];
			m_unitList.dropAt(i);
			break;
		}
	}
	
	m_ptr_serial->dropDevice(packet.senderAddr);
}

/**
* @brief Execute the reception and reaction to an unrecognized message
* @param packet Packet to analyze
*/
void RfManager::receptNCOM(RfPacket packet)
{
	//TODO: Implement a retry system, if there's time
}

/**
* @brief Redirects a received packet to it's corresponding function
* @param packet Packet to pass to a function
*/
void RfManager::receptFunction(RfPacket packet)
{
	if(strncmp(packet.instructHead, "PING", HEAD_LEN) == 0)
		receptPING(packet);
	else if(strncmp(packet.instructHead, "PONG", HEAD_LEN) == 0)
		receptPONG(packet);
	else if(strncmp(packet.instructHead, "NCON", HEAD_LEN) == 0)
		receptNCON(packet);
	else if(strncmp(packet.instructHead, "CPOS", HEAD_LEN) == 0)
		receptCPOS(packet);
	else if(strncmp(packet.instructHead, "ABAT", HEAD_LEN) == 0)
		receptABAT(packet);
	else if(strncmp(packet.instructHead, "LBAT", HEAD_LEN) == 0)
		receptLBAT(packet);
	else if(strncmp(packet.instructHead, "ALRT", HEAD_LEN) == 0)
		receptALRT(packet);
	else if(strncmp(packet.instructHead, "CSTT", HEAD_LEN) == 0)
		receptCSTT(packet);
	else if(strncmp(packet.instructHead, "SDON", HEAD_LEN) == 0)
		receptSDON(packet);
	else if(strncmp(packet.instructHead, "DROP", HEAD_LEN) == 0)
		receptDROP(packet);
	else if(strncmp(packet.instructHead, "NCOM", HEAD_LEN) == 0)
		receptNCOM(packet);
}

/**
* @brief Send a ping message to a certain address
* @param dest Address to send the message to
*/
void RfManager::sendPING(char* dest)
{
	RfPacket packetToSend;

	memcpy(packetToSend.targetAddr, dest, getAddrLen());

	packetToSend.senderAddr[ADDR_PIPE_IDX] = dest[ADDR_PIPE_IDX];
	packetToSend.senderAddr[ADDR_FUNC_IDX] = dest[ADDR_FUNC_IDX];
	packetToSend.senderAddr[ADDR_COMM_ID_IDX] = m_commId;
	packetToSend.senderAddr[ADDR_DEVICE_TYPE_IDX] = m_commDeviceType;
	packetToSend.senderAddr[ADDR_STATUS_IDX] = dest[ADDR_STATUS_IDX];
	
	memcpy(packetToSend.instructHead, "PING", HEAD_LEN);	
	memcpy(packetToSend.instructBody, packetToSend.senderAddr, getAddrLen());
	
	packetToSend.instructBody[getAddrLen()] = '0';
	for(uint8_t i = 0; i < m_unitList.size(); ++i)
		if(strncmp(dest, m_unitList[i].address, getAddrLen()))
			packetToSend.instructBody[getAddrLen()] = '1';
		
	memset(packetToSend.instructBody + getAddrLen() + 1, DONT_CARE_CHAR, sizeof(packetToSend.instructBody) - ((getAddrLen()) + 1));
		
	sendPacket(packetToSend);
}

/**
* @brief Send a pong message to a certain address
* @param dest Address to send the message to
*/
void RfManager::sendPONG(char* dest)
{
	RfPacket packetToSend;

	memcpy(packetToSend.targetAddr, dest, getAddrLen());

	packetToSend.senderAddr[ADDR_PIPE_IDX] = dest[ADDR_PIPE_IDX];
	packetToSend.senderAddr[ADDR_FUNC_IDX] = dest[ADDR_FUNC_IDX];
	packetToSend.senderAddr[ADDR_COMM_ID_IDX] = m_commId;
	packetToSend.senderAddr[ADDR_DEVICE_TYPE_IDX] = m_commDeviceType;
	packetToSend.senderAddr[ADDR_STATUS_IDX] = dest[ADDR_STATUS_IDX];
	
	memcpy(packetToSend.instructHead, "PONG", HEAD_LEN);
	
	memcpy(packetToSend.instructBody, packetToSend.senderAddr, MAX_ADDR_LEN);
	
	packetToSend.instructBody[getAddrLen()] = '0';
	for(uint8_t i = 0; i < m_unitList.size(); ++i)
		if(strncmp(dest, m_unitList[i].address, getAddrLen()))
			packetToSend.instructBody[getAddrLen()] = '1';
	
	memset(packetToSend.instructBody + MAX_ADDR_LEN + 1, DONT_CARE_CHAR, sizeof(packetToSend.instructBody) - (getAddrLen() + 1));
		
	sendPacket(packetToSend);	
}

/**
* @brief Send a connexion setting message to a certain address
* @param dest Address to send the message to
* @param dataPipeIdx Index of the pipe to which the new connexion is assigned
*/
void RfManager::sendSCON(char* dest, uint8_t dataPipeIdx)
{
	RfPacket packetToSend;
	Unit newUnit;
	
	memcpy(newUnit.address, dest, getAddrLen() + 1);
	newUnit.assignedPipe = dataPipeIdx;
	newUnit.awaitingPong = 0;
	newUnit.secSinceLastMsg = 0;
	newUnit.secSincePing = 0;
	newUnit.secSinceSequ = 0;
	newUnit.sequExecuting = 0;
	m_unitList.append(newUnit);
		
	snprintf(packetToSend.targetAddr,  getAddrLen(), dest);
	sprintf( packetToSend.senderAddr,"%c%c%c%c%c", '0', FUNC_SWEEP, m_commId, m_commDeviceType, '0');
	sprintf(packetToSend.instructHead, "SCON");
	getPipAddr(dataPipeIdx, packetToSend.instructBody);
	memset(packetToSend.instructBody + getAddrLen(), DONT_CARE_CHAR, BODY_LEN - ((getAddrLen()) + 1));
	
	sendPacket(packetToSend);
	m_ptr_serial->recvNCON(newUnit.address, dataPipeIdx);
}

/**
* @brief Send a busy connexion message to a certain address
* @param dest Address to send the message to
*/
void RfManager::sendBCON(char* dest)
{
	RfPacket packetToSend;

	memcpy(packetToSend.targetAddr, dest, getAddrLen());
	getPipAddr(0, packetToSend.senderAddr);
	memcpy(packetToSend.instructHead, "BCON", HEAD_LEN);
	memset(packetToSend.instructBody, DONT_CARE_CHAR, BODY_LEN);	

	sendPacket(packetToSend);
}

/**
* @brief Send a target position/sequence message to a certain address
* @param dest Address to send the message to
* @param sequ Code of the sequence to associate
* @param sequSpec Specification for the sequence
* @param code Array corresponding to the position code to associate with the sequence
*/
void RfManager::sendTPOS(char* dest, char sequ, char sequSpec, char* code)
{
	RfPacket packetToSend;

	memcpy(packetToSend.targetAddr, dest, getAddrLen());
	getPipAddr((dest[0] - '0'), packetToSend.senderAddr);
	memcpy(packetToSend.instructHead, "TPOS", HEAD_LEN + 1);
	memcpy(packetToSend.instructBody, (char*)code, 3);
	packetToSend.instructBody[3] = sequ;
	packetToSend.instructBody[4] = sequSpec;
	memset(packetToSend.instructBody + 5, DONT_CARE_CHAR, BODY_LEN - 5);

	sendPacket(packetToSend);
}
	
/**
* @brief Send a current state request message to a certain address
* @param dest Address to send the message to
*/
void RfManager::sendRSTT(char* dest)
{
	RfPacket packetToSend;

	memcpy(packetToSend.targetAddr, dest, MAX_ADDR_LEN);
	genReplyAddr(dest[0], FUNC_UNIT, m_commId, m_commDeviceType, '0', packetToSend.senderAddr);
	memcpy(packetToSend.instructHead, "RSTT", HEAD_LEN);
	memset(packetToSend.instructBody, DONT_CARE_CHAR, BODY_LEN);

	sendPacket(packetToSend);
}

/**
* @brief Send a semi-automatic move set message to a certain address (implementation not well defined yet)
* @param dest Address to send the message to
* @param move_1 First movement specification
* @param move_2 Second movement specification
* @param move_3 Third movemnt specification
* @param move_4 Fourth movement specification
*/
void RfManager::sendDEPL(char* dest, uint8_t move_1, uint8_t move_2, uint8_t move_3, uint8_t move_4)
{
	RfPacket packetToSend;

	memcpy(packetToSend.targetAddr, dest, MAX_ADDR_LEN);
	genReplyAddr(dest[0], FUNC_UNIT, m_commId, m_commDeviceType, '0', packetToSend.senderAddr);
	memcpy(packetToSend.instructHead, "DEPL", HEAD_LEN);
	packetToSend.instructBody[0] = (char) move_1;
	packetToSend.instructBody[1] = (char) move_2;
	packetToSend.instructBody[2] = (char) move_3;
	packetToSend.instructBody[3] = (char) move_4;
	memset(packetToSend.instructBody + 4, DONT_CARE_CHAR, BODY_LEN - 4);

	sendPacket(packetToSend);	
}

/**
* @brief Send an operation mode change message to a certain address
* @param dest Address to send the message to
* @param mode Mode to set the targeted unit to
*/
void RfManager::sendMODE(char* dest, uint8_t mode)
{
	RfPacket packetToSend;

	memcpy(packetToSend.targetAddr, dest, MAX_ADDR_LEN);
	genReplyAddr(dest[0], FUNC_UNIT, m_commId, m_commDeviceType, '0', packetToSend.senderAddr);
	memcpy(packetToSend.instructHead, "DEPL", HEAD_LEN);
	packetToSend.instructBody[0] = (char) mode;
	memset(packetToSend.instructBody + 1, DONT_CARE_CHAR, BODY_LEN - 1);

	sendPacket(packetToSend);	
}

/**
* @brief Send a manual move set message to a certain address
* @param dest Address to send the message to
* @param moveSet Array corresponding to the movement specification list
*/
void RfManager::sendMOVE(char* dest, uint8_t* moveSet)
{
	RfPacket packetToSend;

	memcpy(packetToSend.targetAddr, dest, MAX_ADDR_LEN);
	genReplyAddr(dest[0], FUNC_UNIT, m_commId, m_commDeviceType, '0', packetToSend.senderAddr);
	memcpy(packetToSend.instructHead, "MOVE", HEAD_LEN);
	memcpy(packetToSend.instructBody, moveSet, BODY_LEN);

	sendPacket(packetToSend);
}

/**
* @brief Send a instant sequence execution message to a certain address
* @param dest Address to send the message to
* @param sequCode Code of the sequence to execute
* @param sequSpecifics Specifications of the sequence to execute
*/
void RfManager::sendSEQU(char* dest, char sequCode, char sequSpecifics)
{
	RfPacket packetToSend;

	memcpy(packetToSend.targetAddr, dest, MAX_ADDR_LEN);
	genReplyAddr(dest[0], FUNC_UNIT, m_commId, m_commDeviceType, '0', packetToSend.senderAddr);
	memcpy(packetToSend.instructHead, "SEQU", HEAD_LEN);
	packetToSend.instructBody[0] = sequCode;
	packetToSend.instructBody[1] = sequSpecifics;
	memset(packetToSend.instructBody + 2, DONT_CARE_CHAR, BODY_LEN - 2);
	
	for(uint8_t i = 0; i < m_unitList.size(); ++i)
	{
		if(strncmp(dest, m_unitList[i].address, getAddrLen()) == 0)
		{
			m_unitList[i].sequExecuting = true;	
			break;
		}
	}

	sendPacket(packetToSend);
}

/**
* @brief Send a connexion drop message to a certain address
* @param dest Address to send the message to
*/
void RfManager::sendDROP(char* dest)
{
	RfPacket packetToSend;

	memcpy(packetToSend.targetAddr, dest, MAX_ADDR_LEN);
	genReplyAddr(dest[0], FUNC_UNIT, m_commId, m_commDeviceType, '0', packetToSend.senderAddr);
	memcpy(packetToSend.instructHead, "DROP", HEAD_LEN);
	memset(packetToSend.instructBody, DONT_CARE_CHAR, BODY_LEN);

	sendPacket(packetToSend);
	
	m_ptr_serial->dropDevice(dest);
}

/**
* @brief Send an invalid received message to a certain address
* @param dest Address to send the message to
*/
void RfManager::sendNCOM(char* dest)
{
	RfPacket packetToSend;

	memcpy(packetToSend.targetAddr, dest, MAX_ADDR_LEN);
	genReplyAddr(dest[0], FUNC_UNIT, m_commId, m_commDeviceType, '0', packetToSend.senderAddr);
	memcpy(packetToSend.instructHead, "SEQU", HEAD_LEN);
	memset(packetToSend.instructBody, DONT_CARE_CHAR, BODY_LEN);

	sendPacket(packetToSend);
}

/**
* @brief Send a ping message to a certain unit
* @param index Index on the connected units to send the message to
*/
void RfManager::sendPING(uint8_t index)
{
	if(m_unitList.size() >= index)
		sendPING(m_unitList[index].address);
}
	
/**
* @brief Send a pong message to a certain unit
* @param index Index on the connected units to send the message to
*/
void RfManager::sendPONG(uint8_t index)
{
	if(m_unitList.size() >= index)
		sendPONG(m_unitList[index].address);	
}

/**
* @brief Send a connexion setting message to a certain unit
* @param index Index on the connected units to send the message to
* @param dataPipeIdx Index of the pipe to which the new connexion is assigned
*/
void RfManager::sendSCON(uint8_t index, uint8_t dataPipeIdx)
{
	if(m_unitList.size() >= index)
		sendSCON(m_unitList[index].address, dataPipeIdx);
}

/**
* @brief Send a busy connexion message to a certain unit
* @param index Index on the connected units to send the message to
*/
void RfManager::sendBCON(uint8_t index)
{
	if(m_unitList.size() >= index)
		sendBCON(m_unitList[index].address);	
}

/**
* @brief Send a target position/sequence message to a certain unit
* @param index Index on the connected units to send the message to
* @param sequ Code of the sequence to associate
* @param sequSpec Specification for the sequence
* @param code Array corresponding to the position code to associate with the sequence
*/
void RfManager::sendTPOS(uint8_t index, char sequ, char sequSpec, char* code)
{
	if(m_unitList.size() >= index)
		sendTPOS(m_unitList[index].address, sequ, sequSpec, code);	
}

/**
* @brief Send a current state request message to a certain unit
* @param index Index on the connected units to send the message to
*/
void RfManager::sendRSTT(uint8_t index)
{
	if(m_unitList.size() >= index)
		sendRSTT(m_unitList[index].address);	
}

/**
* @brief Send a semi-automatic move set message to a certain unit (implementation not well defined yet)
* @param index Index on the connected units to send the message to
* @param move_1 First movement specification
* @param move_2 Second movement specification
* @param move_3 Third movement specification
* @param move_4 Fourth movement specification
*/
void RfManager::sendDEPL(uint8_t index, uint8_t move_1, uint8_t move_2, uint8_t move_3, uint8_t move_4)
{
	if(m_unitList.size() >= index)
		sendDEPL(m_unitList[index].address,  move_1, move_2, move_3, move_4);	
}

/**
* @brief Send an operation mode change message to a certain unit
* @param index Index on the connected units to send the message to
* @param mode Mode to set the targeted unit to
*/
void RfManager::sendMODE(uint8_t index, uint8_t mode)
{
	if(m_unitList.size() >= index)
		sendMODE(m_unitList[index].address, mode);	
}

/**
* @brief Send a manual move set message to a certain unit
* @param index Index on the connected units to send the message to
* @param moveSet Array corresponding to the movement specification list
*/
void RfManager::sendMOVE(uint8_t index, uint8_t* moveSet)
{
	if(m_unitList.size() >= index)
		sendMOVE(m_unitList[index].address, moveSet);
}

/**
* @brief Send a instant sequence execution message to a certain unit
* @param index Index on the connected units to send the message to
* @param sequCode Code of the sequence to execute
* @param sequSpecifics Specifications of the sequence to execute
*/
void RfManager::sendSEQU(uint8_t index, char sequCode, char sequSpecifics)
{
	if(m_unitList.size() >= index)
		sendSEQU(m_unitList[index].address, sequCode, sequSpecifics);
}

/**
* @brief Send a connexion drop message to a certain unit
* @param index Index on the connected units to send the message to
*/
void RfManager::sendDROP(uint8_t index)
{
	if(m_unitList.size() >= index)
		sendDROP(m_unitList[index].address);
}

/**
* @brief Send an invalid received message to a certain unit
* @param index Index on the connected units to send the message to
*/
void RfManager::sendNCOM(uint8_t index)
{
	if(m_unitList.size() >= index)
		sendNCOM(m_unitList[index].address);	
}

/**
* @brief Getter of a pointer to the co-dependent SerialManager instance
* @return Pointer to the co-dependent SerialManager instance
*/
SerialManager* RfManager::serial()
{
	return m_ptr_serial;
}
    
/**
* @brief Setter of the pointer to the co-dependent SerialManager instance
* @param ptr_serial New pointer to the co-dependent SerialManager instance
*/
void RfManager::setSerial(SerialManager* ptr_serial)
{
	m_ptr_serial = ptr_serial;
}