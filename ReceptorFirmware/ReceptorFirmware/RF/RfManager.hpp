/**
* @file RfManager.hpp
* @brief Header file for the RfManager class
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @version 1.0.0
* @date February 17th, 2018
*
* The RfManager class provides control over the nRF24L01+ module
* and the MercuryBot communication protocol.
*
*/

#ifndef RFMANAGER_HPP_
#define RFMANAGER_HPP_

/**
* @def TX_CONFIG
* @brief Layout of the CONFIG register to put the module in transmission mode
*/
#define TX_CONFIG ((1<<PWR_UP) | (1<<CRCO) | (1<<EN_CRC) | (1<<MASK_TX_DS) | (1<<MASK_RX_DR) | (1<<MASK_MAX_RT))
/**
* @def RX_CONFIG
* @brief Layout of the CONFIG register to put the module in reception mode
*/
#define RX_CONFIG ((1<<PWR_UP) | (1<<CRCO) | (1<<EN_CRC) | (1<<MASK_TX_DS) | (1<<MASK_MAX_RT) | (1<<PRIM_RX))

/**
* @def FINDING_PORT_IDX
* @brief Index of the data pipe allocated to sweep for addresses
*/
#define FINDING_PORT_IDX 0
/**
* @def MAX_UNITS
* @brief Maximum units on a network (limited to 35 temporarily for debugging purposes)
*/
#define MAX_UNITS 35
/**
* @def MAX_UNITS_PER_PIPE
* @brief Maximum units on a single data pipe
*/
#define MAX_UNITS_PER_PIPE (MAX_UNITS/(MAX_DATA_PIPES - 1))
/**
* @def OPERATING_FREQUENCY_MHZ
* @brief Operating frequency for the protocol
*/
#define OPERATING_FREQUENCY_MHZ 2489

/**
* @def COMM_ID_NOT_INIT
* @brief Default communication ID if uninitialized
*/
#define COMM_ID_NOT_INIT '*'

/**
* @def ADDR_PIPE_IDX
* @brief Index of the address allocated for the pipe's index
*/
#define ADDR_PIPE_IDX 0
/**
* @def ADDR_FUNC_IDX
* @brief Index of the address allocated for the pipe's function 
*/
#define ADDR_FUNC_IDX 1
/**
* @def ADDR_COMM_ID_IDX
* @brief Index of the address allocated for the pipe's communication ID
*/
#define ADDR_COMM_ID_IDX 2
/**
* @def ADDR_DEVICE_TYPE_IDX
* @brief Index of the address allocated for the pipe's device type
*/
#define ADDR_DEVICE_TYPE_IDX 3
/**
* @def ADDR_STATUS_IDX
* @brief Index of the address allocated for the pipe's current status
*/
#define ADDR_STATUS_IDX 4

/**
* @def DONT_CARE_CHAR
* @brief Character in telecommunication payloads for characters to ignore
*/
#define DONT_CARE_CHAR '#'

/**
* @def FUNC_NOTINIT
* @brief Function not yet defined for pipe
*/
#define FUNC_NOTINIT '0'
/**
* @def FUNC_UNIT
* @brief Function is communication between Receptor and units for the pipe
*/
#define FUNC_UNIT '1'
/**
* @def FUNC_SWEEP
@brief Function is sweeping for finding a Receptor to connect to
*/
#define FUNC_SWEEP '2'
/**
* @def FUNC_ADDR
* @brief Function for the pipe is to intercept and reply if an address is already taken for a unit
*/
#define FUNC_ADDR '3'
/**
* @def FUNC_OTHER
* @brief Function for the pipe is something else
*/
#define FUNC_OTHER '4'
/**
* @def FUNC_NOTINIT
* @brief The pipe is blocked, has a denied access
*/
#define FUNC_BLOCKED '5'

/**
* @def DEVICE_TYPE_NOT_INIT
* @brief The device type is undefined
*/
#define DEVICE_TYPE_NOT_INIT '0'
/**
* @def DEVICE_TYPE_UNIT
* @brief The device type is a unit
*/
#define DEVICE_TYPE_UNIT 'U'
/**
* @def DEVICE_TYPE_RECEPT
* @brief The device type is a Receptor
*/
#define DEVICE_TYPE_RECEPT 'R'

/**
* @def STATUS_OK
* @brief The status of the pipe is normal
*/
#define STATUS_OK '0'
/**
* @def STATUS_ERROR
* @brief The status of the pipe is not normal/in error
*/
#define STATUS_ERROR '1'

/**
* @def MAX_PONG_DELAY_SEC
* @brief Maximum delay (in seconds) allowed for a pong back before time out
*/
#define MAX_PONG_DELAY_SEC 5
/**
* @def MAX_INTER_DELAY_SEC
* @brief Maximum delay (in seconds) allowed without activity with a certain unit
*/
#define MAX_INTER_DELAY_SEC 60
/**
* @def MAX_SEQU_DELAY_SEC
* @brief Maximum delay (in seconds) allowed without activity with a certain unit since the beginning of a sequence
*/
#define MAX_SEQU_DELAY_SEC 45

#include <avr/interrupt.h>

#include "NRF24L01.h"
#include "ControlRf.hpp"
#include "LinkedList.hpp"
#include "Unit.h"
#include "RfPacket.h"

class SerialManager;

typedef void task;

/**
* @class RfManager
* @brief Provides control over the nRF24L01+ module and the MercuryBot communication protocol
*/
class RfManager: public ControlRf {
	
	private:
	
	/**
	* @brief List of connected units on the Receptor's network
	*/
	LinkedList<Unit> m_unitList;
	
	/**
	* @brief Value to check if the Rf module is initialized according to the Receptor's configuration
	*/
	bool m_init;
	
	/**
	* @brief Value to confirm if the RfManager needs to print it's received and sent packets over serial port
	*/
	bool m_printPackets;
	
	/**
	* @brief Array with the number of units connected on each pipe
	*/
	uint8_t m_deviceNumOnPipe[6];
	
	/**
	* @brief Current communication ID assigned by the Operator
	*/
	char m_commId;
		
	/**
	* @brief Device type
	*/
	char m_commDeviceType;
	
	/**
	* @brief Buffer for message for reception
	*/
	RingBuffer<char> m_buffer;
	
	protected:
	/**
	* @brief Pointer to a SerialManager instance to allow co-dependency with the class
	*/
	SerialManager* m_ptr_serial;
		
	public:
	
	/**
	* @brief The class' base constructor
	*/
	RfManager();
	
	/**
	* @brief The class' base destructor
	*/
	~RfManager();
	
	/**
	* @brief Getter of the pointer to the receiving buffer
	* @return Pointer to the receiving buffer
	*/
	RingBuffer<char>* buffer(void);
	
	/**
	* @brief Send data to a specific address 
	* @param a_addr Address to send the data to
	* @param a_data Data to send over
	*/
	void sendData(char* a_addr, char* a_data);
	
	/**
	* @brief Reset and disable a listening pipe
	* @param idx Index of the targeted pipe
	*/
	void dropPipe(uint8_t idx);
	
	/**
	* @brief Enable and set a pipe's specifics
	* @param idx Index of the targeted pipe
	* @param a_addr New address to set
	*/
	void setPipe(uint8_t idx, char* a_addr);
	
	/**
	* @brief Initializes the RfModule to fit the telecommunication protocol
	*/
	void init(void);
	
	/**
	* @brief Main task to execute
	*/
	task mainTask(void);
	
	/**
	* @brief Secondary task to execute when the external interrupt is triggered
	*/
	void intTask(void);
		
	/**
	* @brief Secondary task to execute periodically
	*/
	void timerSecTask(void);
	
	/**
	* @brief Gets the number of pipes that aren't overloaded yet
	* @return Number of pipes that aren't overloaded yet
	*/
	uint8_t getFreePortsQty(void);
	
	/**
	* @brief Confirms if a packet has been received and is waiting to be read
	* @return 0 if no packet has been received, 1 if a packet has been received and is waiting reading
	*/
	bool receivedPacket(void);
	
	/**
	* @brief Extracts a packet from the internal registers and casts it into an RfPacket structure
	* @return Read and formatted packet
	*/
	RfPacket extractPacket(void);
	
	/**
	* @brief Sends a packet on the network
	* @param Packet Packet to send
	*/
	void sendPacket(RfPacket packet);
	
	/**
	* @brief Setter for the packet printing confirmation
	* @param data 0 to disable packet printing over serial port, 1 to enable it
	*/
	void setPrintingPackets(bool data);	
	
	/**
	* @brief Generates an address in the form of a string based on the passed parameters 
	* @param index Index of the pipe
	* @param func Function of the pipe
	* @param commId Communication ID of the device
	* @param deviceType Type of the device
	* @param status Status of the pipe
	* @param a_outData Output of the generated address
	*/
	void genReplyAddr(char index, char func, char commId, char deviceType, char status, char* a_outData);
	
	/**
	* @brief Getter of the total number of connected units to the Receptor
	* @return The total number of connected units to the Receptor
	*/
	uint8_t getDeviceNum(void) const;
	
	/**
	* @brief Getter of the number of connected units one of the Receptor's pipes
	* @param idx Index of the targeted pipe
	* @return The number of connected units to the Receptor's pipe
	*/
	uint8_t getDeviceNumOnPipe(uint8_t idx) const;
	
	/**
	* @brief Setter for the communication ID of the Receptor, with corresponding registers transformation
	* @param ID New communication ID to set  
	*/
	void setCommID(char ID);
	
	/**
	* @brief Getter of the communication ID of the Receptor
	* @return The communication ID of the Receptor
	*/
	char getCommID(void) const;
	
	/**
	* @brief Confirms if the RF module has been correctly initialized
	* @return 0 if it hasn't been initialized, 1 if it has
	*/
	bool isInit(void) const;
	
	/**
	* @brief Getter to a pointer to the units list
	* @return The pointer to the units list
	*/
	LinkedList<Unit>* unitList(void);
	
	/**
	* @brief Execute the reception and reaction to a ping message
	* @param packet Packet to analyze
	*/
	void receptPING(RfPacket packet);
	
	/**
	* @brief Execute the reception and reaction to a pong (reply to ping) message
	* @param packet Packet to analyze
	*/
	void receptPONG(RfPacket packet);
	
	/**
	* @brief Execute the reception and reaction to a new connection request message
	* @param packet Packet to analyze
	*/
	void receptNCON(RfPacket packet);
	
	/**
	* @brief Execute the reception and reaction to a position update message
	* @param packet Packet to analyze
	*/
	void receptCPOS(RfPacket packet);
	
	/**
	* @brief Execute the reception and reaction to a low battery alert message
	* @param packet Packet to analyze
	*/
	void receptABAT(RfPacket packet);
	
	/**
	* @brief Execute the reception and reaction to a battery level message
	* @param packet Packet to analyze
	*/
	void receptLBAT(RfPacket packet);
	
	/**
	* @brief Execute the reception and reaction to a state alert message
	* @param packet Packet to analyze
	*/
	void receptALRT(RfPacket packet);
	
	/**
	* @brief Execute the reception and reaction to a current state update message
	* @param packet Packet to analyze
	*/
	void receptCSTT(RfPacket packet);
	
	/**
	* @brief Execute the reception and reaction to a done sequence message
	* @param packet Packet to analyze
	*/
	void receptSDON(RfPacket packet);
	
	/**
	* @brief Execute the reception and reaction to a dropped connexion message
	* @param packet Packet to analyze
	*/
	void receptDROP(RfPacket packet);
	
	/**
	* @brief Execute the reception and reaction to an unrecognized message
	* @param packet Packet to analyze
	*/
	void receptNCOM(RfPacket packet);
	
	/**
	* @brief Redirects a received packet to it's corresponding function
	* @param packet Packet to pass to a function
	*/
	void receptFunction(RfPacket packet);
	
	/**
	* @brief Send a ping message to a certain address
	* @param dest Address to send the message to
	*/
	void sendPING(char* dest);
	
	/**
	* @brief Send a pong message to a certain address
	* @param dest Address to send the message to
	*/
	void sendPONG(char* dest);
	
	/**
	* @brief Send a connexion setting message to a certain address
	* @param dest Address to send the message to
	* @param dataPipeIdx Index of the pipe to which the new connexion is assigned
	*/
	void sendSCON(char* dest, uint8_t dataPipeIdx);
	
	/**
	* @brief Send a busy connexion message to a certain address
	* @param dest Address to send the message to
	*/
	void sendBCON(char* dest);
	
	/**
	* @brief Send a target position/sequence message to a certain address
	* @param dest Address to send the message to
	* @param sequ Code of the sequence to associate
	* @param sequSpec Specification for the sequence
	* @param code Array corresponding to the position code to associate with the sequence
	*/
	void sendTPOS(char* dest,  char sequ, char sequSpec, char* code);
	
	/**
	* @brief Send a current state request message to a certain address
	* @param dest Address to send the message to
	*/
	void sendRSTT(char* dest);
	
	/**
	* @brief Send a semi-automatic move set message to a certain address (implementation not well defined yet)
	* @param dest Address to send the message to
	* @param move_1 First movement specification
	* @param move_2 Second movement specification
	* @param move_3 Third movemnt specification
	* @param move_4 Fourth movement specification
	*/
	void sendDEPL(char* dest, uint8_t move_1, uint8_t move_2, uint8_t move_3, uint8_t move_4);
	
	/**
	* @brief Send an operation mode change message to a certain address
	* @param dest Address to send the message to
	* @param mode Mode to set the targeted unit to
	*/
	void sendMODE(char* dest, uint8_t mode);
	
	/**
	* @brief Send a manual move set message to a certain address
	* @param dest Address to send the message to
	* @param moveSet Array corresponding to the movement specification list
	*/
	void sendMOVE(char* dest, uint8_t* moveSet);
	
	/**
	* @brief Send a instant sequence execution message to a certain address
	* @param dest Address to send the message to
	* @param sequCode Code of the sequence to execute
	* @param sequSpecifics Specifications of the sequence to execute
	*/
	void sendSEQU(char* dest, char sequCode, char sequSpecifics);
	
	/**
	* @brief Send a connexion drop message to a certain address
	* @param dest Address to send the message to
	*/
	void sendDROP(char* dest);
	
	/**
	* @brief Send an invalid received message to a certain address
	* @param dest Address to send the message to
	*/
	void sendNCOM(char* dest);
	
	/**
	* @brief Send a ping message to a certain unit
	* @param index Index on the connected units to send the message to
	*/
	void sendPING(uint8_t index);
	
	/**
	* @brief Send a pong message to a certain unit
	* @param index Index on the connected units to send the message to
	*/
	void sendPONG(uint8_t index);
	
	/**
	* @brief Send a connexion setting message to a certain unit
	* @param index Index on the connected units to send the message to
	* @param dataPipeIdx Index of the pipe to which the new connexion is assigned
	*/
	void sendSCON(uint8_t index, uint8_t dataPipeIdx);
	
	/**
	* @brief Send a busy connexion message to a certain unit
	* @param index Index on the connected units to send the message to
	*/
	void sendBCON(uint8_t index);
	
	/**
	* @brief Send a target position/sequence message to a certain unit
	* @param index Index on the connected units to send the message to
	* @param sequ Code of the sequence to associate
	* @param sequSpec Specification for the sequence
	* @param code Array corresponding to the position code to associate with the sequence
	*/
	void sendTPOS(uint8_t index,  char sequ, char sequSpec, char* code);
	
	/**
	* @brief Send a current state request message to a certain unit
	* @param index Index on the connected units to send the message to
	*/
	void sendRSTT(uint8_t index);
	
	/**
	* @brief Send a semi-automatic move set message to a certain unit (implementation not well defined yet)
	* @param index Index on the connected units to send the message to
	* @param move_1 First movement specification
	* @param move_2 Second movement specification
	* @param move_3 Third movement specification
	* @param move_4 Fourth movement specification
	*/
	void sendDEPL(uint8_t index, uint8_t move_1, uint8_t move_2, uint8_t move_3, uint8_t move_4);
	
	/**
	* @brief Send an operation mode change message to a certain unit
	* @param index Index on the connected units to send the message to
	* @param mode Mode to set the targeted unit to
	*/
	void sendMODE(uint8_t index, uint8_t mode);
	
	/**
	* @brief Send a manual move set message to a certain unit
	* @param index Index on the connected units to send the message to
	* @param moveSet Array corresponding to the movement specification list
	*/
	void sendMOVE(uint8_t index, uint8_t* moveSet);
	
	/**
	* @brief Send a instant sequence execution message to a certain unit
	* @param index Index on the connected units to send the message to
	* @param sequCode Code of the sequence to execute
	* @param sequSpecifics Specifications of the sequence to execute
	*/
	void sendSEQU(uint8_t index, char sequCode, char sequSpecifics);
	
	/**
	* @brief Send a connexion drop message to a certain unit
	* @param index Index on the connected units to send the message to
	*/
	void sendDROP(uint8_t index);
	
	/**
	* @brief Send an invalid received message to a certain unit
	* @param index Index on the connected units to send the message to
	*/
	void sendNCOM(uint8_t index);
	
	/**
	* @brief Getter of a pointer to the co-dependent SerialManager instance
	* @return Pointer to the co-dependent SerialManager instance
	*/
	SerialManager* serial();
	
	/**
	* @brief Setter of the pointer to the co-dependent SerialManager instance
	* @param ptr_serial New pointer to the co-dependent SerialManager instance
	*/
	void setSerial(SerialManager* ptr_serial);
};

#endif /* RFMANAGER_HPP_ */